﻿namespace THI_TDUNG
{
    partial class frmThi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cLabel;
            System.Windows.Forms.Label aLabel;
            System.Windows.Forms.Label bLabel;
            System.Windows.Forms.Label cLabel1;
            System.Windows.Forms.Label dLabel;
            this.grbNoiDung = new System.Windows.Forms.GroupBox();
            this.lbND = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioD = new System.Windows.Forms.RadioButton();
            this.bODEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dS = new THI_TDUNG.DS();
            this.radioC = new System.Windows.Forms.RadioButton();
            this.radioB = new System.Windows.Forms.RadioButton();
            this.radioA = new System.Windows.Forms.RadioButton();
            this.grbLuaChon = new System.Windows.Forms.GroupBox();
            this.btnThoat = new System.Windows.Forms.Button();
            this.btnKetthuc = new System.Windows.Forms.Button();
            this.btnCautruoc = new System.Windows.Forms.Button();
            this.btnCausau = new System.Windows.Forms.Button();
            this.btnBatdau = new System.Windows.Forms.Button();
            this.grbTimer = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.bODETableAdapter = new THI_TDUNG.DSTableAdapters.BODETableAdapter();
            this.tableAdapterManager = new THI_TDUNG.DSTableAdapters.TableAdapterManager();
            this.bANGDIEMTableAdapter = new THI_TDUNG.DSTableAdapters.BANGDIEMTableAdapter();
            this.bANGDIEMBindingSource = new System.Windows.Forms.BindingSource(this.components);
            cLabel = new System.Windows.Forms.Label();
            aLabel = new System.Windows.Forms.Label();
            bLabel = new System.Windows.Forms.Label();
            cLabel1 = new System.Windows.Forms.Label();
            dLabel = new System.Windows.Forms.Label();
            this.grbNoiDung.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bODEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS)).BeginInit();
            this.grbLuaChon.SuspendLayout();
            this.grbTimer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bANGDIEMBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // cLabel
            // 
            cLabel.Location = new System.Drawing.Point(0, 0);
            cLabel.Name = "cLabel";
            cLabel.Size = new System.Drawing.Size(100, 23);
            cLabel.TabIndex = 8;
            cLabel.Text = "Đáp án";
            // 
            // aLabel
            // 
            aLabel.AutoSize = true;
            aLabel.Location = new System.Drawing.Point(33, 53);
            aLabel.Name = "aLabel";
            aLabel.Size = new System.Drawing.Size(24, 20);
            aLabel.TabIndex = 8;
            aLabel.Text = "A:";
            // 
            // bLabel
            // 
            bLabel.AutoSize = true;
            bLabel.Location = new System.Drawing.Point(33, 131);
            bLabel.Name = "bLabel";
            bLabel.Size = new System.Drawing.Size(24, 20);
            bLabel.TabIndex = 9;
            bLabel.Text = "B:";
            // 
            // cLabel1
            // 
            cLabel1.AutoSize = true;
            cLabel1.Location = new System.Drawing.Point(583, 53);
            cLabel1.Name = "cLabel1";
            cLabel1.Size = new System.Drawing.Size(24, 20);
            cLabel1.TabIndex = 10;
            cLabel1.Text = "C:";
            // 
            // dLabel
            // 
            dLabel.AutoSize = true;
            dLabel.Location = new System.Drawing.Point(582, 121);
            dLabel.Name = "dLabel";
            dLabel.Size = new System.Drawing.Size(25, 20);
            dLabel.TabIndex = 11;
            dLabel.Text = "D:";
            // 
            // grbNoiDung
            // 
            this.grbNoiDung.Controls.Add(this.lbND);
            this.grbNoiDung.Controls.Add(this.groupBox4);
            this.grbNoiDung.Location = new System.Drawing.Point(10, 147);
            this.grbNoiDung.Name = "grbNoiDung";
            this.grbNoiDung.Size = new System.Drawing.Size(1057, 338);
            this.grbNoiDung.TabIndex = 11;
            this.grbNoiDung.TabStop = false;
            this.grbNoiDung.Text = "Nội dung";
            // 
            // lbND
            // 
            this.lbND.AutoSize = true;
            this.lbND.Location = new System.Drawing.Point(23, 54);
            this.lbND.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbND.Name = "lbND";
            this.lbND.Size = new System.Drawing.Size(127, 20);
            this.lbND.TabIndex = 38;
            this.lbND.Text = "Nội dung câu hỏi";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(dLabel);
            this.groupBox4.Controls.Add(this.radioD);
            this.groupBox4.Controls.Add(cLabel1);
            this.groupBox4.Controls.Add(this.radioC);
            this.groupBox4.Controls.Add(bLabel);
            this.groupBox4.Controls.Add(this.radioB);
            this.groupBox4.Controls.Add(aLabel);
            this.groupBox4.Controls.Add(this.radioA);
            this.groupBox4.Controls.Add(cLabel);
            this.groupBox4.Location = new System.Drawing.Point(6, 128);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1005, 187);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Đáp án";
            // 
            // radioD
            // 
            this.radioD.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.bODEBindingSource, "D", true));
            this.radioD.Location = new System.Drawing.Point(613, 115);
            this.radioD.Name = "radioD";
            this.radioD.Size = new System.Drawing.Size(104, 24);
            this.radioD.TabIndex = 12;
            this.radioD.TabStop = true;
            this.radioD.Text = "radioButton1";
            this.radioD.UseVisualStyleBackColor = true;
            // 
            // bODEBindingSource
            // 
            this.bODEBindingSource.DataMember = "BODE";
            this.bODEBindingSource.DataSource = this.dS;
            // 
            // dS
            // 
            this.dS.DataSetName = "DS";
            this.dS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // radioC
            // 
            this.radioC.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.bODEBindingSource, "C", true));
            this.radioC.Location = new System.Drawing.Point(613, 47);
            this.radioC.Name = "radioC";
            this.radioC.Size = new System.Drawing.Size(104, 24);
            this.radioC.TabIndex = 11;
            this.radioC.TabStop = true;
            this.radioC.Text = "radioButton1";
            this.radioC.UseVisualStyleBackColor = true;
            // 
            // radioB
            // 
            this.radioB.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.bODEBindingSource, "B", true));
            this.radioB.Location = new System.Drawing.Point(63, 125);
            this.radioB.Name = "radioB";
            this.radioB.Size = new System.Drawing.Size(104, 24);
            this.radioB.TabIndex = 10;
            this.radioB.TabStop = true;
            this.radioB.Text = "radioButton1";
            this.radioB.UseVisualStyleBackColor = true;
            // 
            // radioA
            // 
            this.radioA.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.bODEBindingSource, "A", true));
            this.radioA.Location = new System.Drawing.Point(63, 47);
            this.radioA.Name = "radioA";
            this.radioA.Size = new System.Drawing.Size(104, 24);
            this.radioA.TabIndex = 9;
            this.radioA.TabStop = true;
            this.radioA.Text = "radioButton1";
            this.radioA.UseVisualStyleBackColor = true;
            // 
            // grbLuaChon
            // 
            this.grbLuaChon.Controls.Add(this.btnThoat);
            this.grbLuaChon.Controls.Add(this.btnKetthuc);
            this.grbLuaChon.Controls.Add(this.btnCautruoc);
            this.grbLuaChon.Controls.Add(this.btnCausau);
            this.grbLuaChon.Controls.Add(this.btnBatdau);
            this.grbLuaChon.Location = new System.Drawing.Point(395, 22);
            this.grbLuaChon.Name = "grbLuaChon";
            this.grbLuaChon.Size = new System.Drawing.Size(636, 119);
            this.grbLuaChon.TabIndex = 10;
            this.grbLuaChon.TabStop = false;
            this.grbLuaChon.Text = "Lựa chọn";
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(496, 47);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(112, 35);
            this.btnThoat.TabIndex = 38;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnKetthuc
            // 
            this.btnKetthuc.Location = new System.Drawing.Point(137, 47);
            this.btnKetthuc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnKetthuc.Name = "btnKetthuc";
            this.btnKetthuc.Size = new System.Drawing.Size(112, 35);
            this.btnKetthuc.TabIndex = 37;
            this.btnKetthuc.Text = "Kết thúc";
            this.btnKetthuc.UseVisualStyleBackColor = true;
            this.btnKetthuc.Click += new System.EventHandler(this.btnKetthuc_Click);
            // 
            // btnCautruoc
            // 
            this.btnCautruoc.Location = new System.Drawing.Point(257, 47);
            this.btnCautruoc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCautruoc.Name = "btnCautruoc";
            this.btnCautruoc.Size = new System.Drawing.Size(112, 35);
            this.btnCautruoc.TabIndex = 36;
            this.btnCautruoc.Text = "< Câu trước";
            this.btnCautruoc.UseVisualStyleBackColor = true;
            this.btnCautruoc.Click += new System.EventHandler(this.btnCautruoc_Click);
            // 
            // btnCausau
            // 
            this.btnCausau.Location = new System.Drawing.Point(377, 47);
            this.btnCausau.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCausau.Name = "btnCausau";
            this.btnCausau.Size = new System.Drawing.Size(112, 35);
            this.btnCausau.TabIndex = 35;
            this.btnCausau.Text = "Câu sau >";
            this.btnCausau.UseVisualStyleBackColor = true;
            this.btnCausau.Click += new System.EventHandler(this.btnCausau_Click);
            // 
            // btnBatdau
            // 
            this.btnBatdau.Location = new System.Drawing.Point(17, 47);
            this.btnBatdau.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnBatdau.Name = "btnBatdau";
            this.btnBatdau.Size = new System.Drawing.Size(112, 35);
            this.btnBatdau.TabIndex = 34;
            this.btnBatdau.Text = "Bắt đầu";
            this.btnBatdau.UseVisualStyleBackColor = true;
            this.btnBatdau.Click += new System.EventHandler(this.btnBatdau_Click);
            // 
            // grbTimer
            // 
            this.grbTimer.Controls.Add(this.label5);
            this.grbTimer.Controls.Add(this.label4);
            this.grbTimer.Controls.Add(this.label3);
            this.grbTimer.Controls.Add(this.label2);
            this.grbTimer.Controls.Add(this.label1);
            this.grbTimer.Location = new System.Drawing.Point(10, 13);
            this.grbTimer.Name = "grbTimer";
            this.grbTimer.Size = new System.Drawing.Size(328, 128);
            this.grbTimer.TabIndex = 9;
            this.grbTimer.TabStop = false;
            this.grbTimer.Text = "Thời gian";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(187, 44);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 29);
            this.label5.TabIndex = 40;
            this.label5.Text = ":";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(102, 44);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 29);
            this.label4.TabIndex = 39;
            this.label4.Text = ":";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(57, 39);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 52);
            this.label3.TabIndex = 38;
            this.label3.Text = "00 ";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(217, 38);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 33);
            this.label2.TabIndex = 37;
            this.label2.Text = "00 ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(132, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 33);
            this.label1.TabIndex = 36;
            this.label1.Text = "00 ";
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // bODETableAdapter
            // 
            this.bODETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BANGDIEMTableAdapter = this.bANGDIEMTableAdapter;
            this.tableAdapterManager.BODETableAdapter = this.bODETableAdapter;
            this.tableAdapterManager.COSOTableAdapter = null;
            this.tableAdapterManager.GIAOVIEN_DANGKYTableAdapter = null;
            this.tableAdapterManager.GIAOVIENTableAdapter = null;
            this.tableAdapterManager.KHOATableAdapter = null;
            this.tableAdapterManager.LOPTableAdapter = null;
            this.tableAdapterManager.MONHOCTableAdapter = null;
            this.tableAdapterManager.SINHVIENTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = THI_TDUNG.DSTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // bANGDIEMTableAdapter
            // 
            this.bANGDIEMTableAdapter.ClearBeforeFill = true;
            // 
            // bANGDIEMBindingSource
            // 
            this.bANGDIEMBindingSource.DataMember = "BANGDIEM";
            this.bANGDIEMBindingSource.DataSource = this.dS;
            // 
            // frmThi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1078, 535);
            this.Controls.Add(this.grbNoiDung);
            this.Controls.Add(this.grbLuaChon);
            this.Controls.Add(this.grbTimer);
            this.Name = "frmThi";
            this.Text = "frmThi";
            this.Load += new System.EventHandler(this.frmThi_Load);
            this.grbNoiDung.ResumeLayout(false);
            this.grbNoiDung.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bODEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS)).EndInit();
            this.grbLuaChon.ResumeLayout(false);
            this.grbTimer.ResumeLayout(false);
            this.grbTimer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bANGDIEMBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbNoiDung;
        private System.Windows.Forms.Label lbND;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox grbLuaChon;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Button btnKetthuc;
        private System.Windows.Forms.Button btnCautruoc;
        private System.Windows.Forms.Button btnCausau;
        private System.Windows.Forms.Button btnBatdau;
        private System.Windows.Forms.GroupBox grbTimer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private DS dS;
        private System.Windows.Forms.BindingSource bODEBindingSource;
        private DSTableAdapters.BODETableAdapter bODETableAdapter;
        private DSTableAdapters.TableAdapterManager tableAdapterManager;
        private DSTableAdapters.BANGDIEMTableAdapter bANGDIEMTableAdapter;
        private System.Windows.Forms.BindingSource bANGDIEMBindingSource;
        private System.Windows.Forms.RadioButton radioD;
        private System.Windows.Forms.RadioButton radioC;
        private System.Windows.Forms.RadioButton radioB;
        private System.Windows.Forms.RadioButton radioA;
    }
}