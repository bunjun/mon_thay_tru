﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace THI_TDUNG
{
    public partial class frmTruockhithi : Form
    {
        public frmTruockhithi()
        {
            InitializeComponent();
        }


        private void frmTruockhithi_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dS.DS_GVDK' table. You can move, or remove it, as needed.
            this.dS_GVDKTableAdapter.Fill(this.dS.DS_GVDK);
            // TODO: This line of code loads data into the 'dS.GIAOVIEN_DANGKY' table. You can move, or remove it, as needed.
            this.gIAOVIEN_DANGKYTableAdapter.Fill(this.dS.GIAOVIEN_DANGKY);
            if (Program.mGroup == "SINHVIEN")
            {
                getSinhVien(Program.username);

                cmbMalop.Enabled = false;
            }
            if (Program.mGroup == "GIAOVIEN" || Program.mGroup == "COSO")
            {
                grbThongtin.Enabled = false;
                cmbMalop.Enabled = true;
            }
        }


        private void getSinhVien(string id)
        {
            using (var connection = new SqlConnection(Program.connstr))
            {
                connection.Open();
                string strLenh = string.Format("EXEC SP_TIMSV '{0}'", id);
                using (var command = new SqlCommand(strLenh, connection))
                {
                    command.CommandType = CommandType.Text;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string result = reader.GetString(0);
                            lbhoten.Text = result;
                            string result1 = reader.GetString(1);
                            lbmalop.Text = result1;
                            string result2 = reader.GetString(2);
                            lbtenlop.Text = result2;

                        }
                    }
                }
                connection.Close();
            }
        }
        private bool checkKtraLanthi (string masv , string mamh, string lan)
        {
            string strLenh = string.Format("EXEC SP_KTLANTHI '{0}','{1}',{2}", masv, mamh, lan);
            //isExist
            bool exist = false;
            using (SqlConnection connection = new SqlConnection(Program.connstr))
            {
                connection.Open();
                SqlCommand sqlcmd = new SqlCommand(strLenh, connection);
                sqlcmd.CommandType = CommandType.Text;
                try
                {
                    SqlDataReader myreader = sqlcmd.ExecuteReader();
                    exist = true;
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);
                    //MessageBox.Show("",e.Errors.ToString());
                    exist = false;
                }
            }
            return exist;
        }
        private bool checkGVDK(string malop, string mamh, string ngaythi, string lan)
        {
            Program.maMH = cbMAMH.SelectedValue.ToString();
            string strLenh = string.Format("EXEC sp_timbode '{0}', '{1}',{2},'{3}'", mamh, ngaythi, lan, malop);
            //isExist
            bool exist = false;
            using (SqlConnection connection = new SqlConnection(Program.connstr))
            {
                connection.Open();
                SqlCommand sqlcmd = new SqlCommand(strLenh, connection);
                sqlcmd.CommandType = CommandType.Text;
                try
                {
                    SqlDataReader myreader = sqlcmd.ExecuteReader();
                    using (myreader)
                    {
                        while (myreader.Read())
                        {

                            int socau = myreader.GetInt16(0);// lỗi hk thể cast int16 sang string đc, nên getint
                            string result = myreader.GetString(1);
                            int thoigian = myreader.GetInt16(2);

                            Program.socau = socau;// kiểu int
                            Program.trinhdo1 = result;
                            Program.thoigianthi = thoigian;// kiểu int
                            //cast string sang int de gan sang program.lan
                            Program.lan = Int32.Parse(lan);
                            // MessageBox.Show("TIM BO DE" + socau + "" + result + "" + thoigian);
                            MessageBox.Show("Số câu thi: " + socau + "\nThời gian: " + thoigian + " phút" + "\nTrình độ:" + result);
                            if (Program.trinhdo1 == "A")
                            {
                                Program.trinhdo2 = "B";
                            }
                            else if (Program.trinhdo1 == "B")
                            {
                                Program.trinhdo2 = "C";
                            }
                            else
                            {
                                Program.trinhdo2 = "C";
                            }
                        }
                    }
                    exist = true;
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);
                    //MessageBox.Show("",e.Errors.ToString());
                    exist = false;
                }
            }
            return exist;
        }

        private void btnThi_Click_1(object sender, EventArgs e)
        {
            Program.maLop = lbmalop.Text;
            String date1 = deNGAYTHI.Text;
            DateTime date = DateTime.ParseExact(date1, new string[] { "dd/MM/yyyy" }, CultureInfo.CurrentCulture, DateTimeStyles.AllowWhiteSpaces);
            string date2 = string.Format("{0: MM/dd/yyyy}", date);
               MessageBox.Show(date2);
            Program.ngay = date2;

            Program.lan = Convert.ToInt32(seLAN.Value);
            if (Program.mGroup == "SINHVIEN")
            {
                if (checkGVDK(lbmalop.Text, cbMAMH.SelectedValue.ToString(), date2, Convert.ToString(seLAN.Value)) == true)
                {

                    if (checkKtraLanthi(Program.username,cbMAMH.SelectedValue.ToString(), Convert.ToString(seLAN.Value)) == true)
                    {
                        Program.frmThi = new frmThi();
                        this.Visible = false;
                        Program.frmThi.ShowDialog();
                        this.Visible = true;
                    }
                    
                }
                else
                {
                    MessageBox.Show("Thông tin chưa chính xác, vui lòng kiểm tra lại", string.Empty, MessageBoxButtons.OK);
                    return;
                }
            }
            if (Program.mGroup == "GIAOVIEN" || Program.mGroup == "COSO")
            {
                if (checkGVDK(cmbMalop.SelectedValue.ToString().Trim(), cbMAMH.SelectedValue.ToString().Trim(), date2, Convert.ToString(seLAN.Value)) == true)
                {
                    Program.frmThi = new frmThi();
                    this.Visible = false;
                    Program.frmThi.ShowDialog();
                    this.Visible = true;
                }
                else
                {
                    MessageBox.Show("Thông tin chưa chính xác, vui lòng kiểm tra lại", string.Empty, MessageBoxButtons.OK);
                    return;
                }
            }
        }

        private void btnThoat_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
