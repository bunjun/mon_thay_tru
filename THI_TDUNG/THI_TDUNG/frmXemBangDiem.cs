﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace THI_TDUNG
{
    public partial class frmXemBangDiem : Form
    {
        public frmXemBangDiem()
        {
            InitializeComponent();
        }

        private void frmXemBangDiem_Load(object sender, EventArgs e)
        {
            dS.EnforceConstraints = false;
            // Nhận giá trị cmbchuyencoso
            Program.connstr = "Data Source=" + Program.servername + ";Initial Catalog=" +
                      Program.database + ";User ID=" +
                      Program.mlogin + ";password=" + Program.password;
            // TODO: This line of code loads data into the 'dS.GIAOVIEN_DANGKY' table. You can move, or remove it, as needed.
            this.gIAOVIEN_DANGKYTableAdapter.Fill(this.dS.GIAOVIEN_DANGKY);
            // TODO: This line of code loads data into the 'dS.MONHOC' table. You can move, or remove it, as needed.
            this.mONHOCTableAdapter.Fill(this.dS.MONHOC);
            // TODO: This line of code loads data into the 'dS.LOP' table. You can move, or remove it, as needed.
            this.lOPTableAdapter.Fill(this.dS.LOP);



        }

        private void btnInBd_Click(object sender, EventArgs e)
        {
            RP_INBDMH rp = new RP_INBDMH(cmbMalop.SelectedValue.ToString(), cmbMamh.SelectedValue.ToString(), Int16.Parse(cmbLan.Text));
            rp.lbLop.Text = "Lớp: " + cmbMalop.Text;
            rp.lbMh.Text = "Môn học: " + cmbMamh.Text;
            ReportPrintTool print = new ReportPrintTool(rp);
            print.ShowPreviewDialog();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
