﻿namespace THI_TDUNG
{
    partial class frmDkThi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label mAGVLabel;
            System.Windows.Forms.Label mAMHLabel;
            System.Windows.Forms.Label mALOPLabel;
            System.Windows.Forms.Label tRINHDOLabel;
            System.Windows.Forms.Label nGAYTHILabel;
            System.Windows.Forms.Label lANLabel;
            System.Windows.Forms.Label sOCAUTHILabel;
            System.Windows.Forms.Label tHOIGIANLabel;
            this.btnUndo = new System.Windows.Forms.Button();
            this.grbNoiDung = new System.Windows.Forms.GroupBox();
            this.txtMAGV = new DevExpress.XtraEditors.TextEdit();
            this.gIAOVIEN_DANGKYBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dS = new THI_TDUNG.DS();
            this.mAMHComboBox = new System.Windows.Forms.ComboBox();
            this.mONHOCBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mALOPComboBox = new System.Windows.Forms.ComboBox();
            this.lOPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tRINHDOComboBox = new System.Windows.Forms.ComboBox();
            this.nGAYTHIDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.speLan = new DevExpress.XtraEditors.SpinEdit();
            this.sOCAUTHITextBox = new System.Windows.Forms.TextBox();
            this.tHOIGIANTextBox = new System.Windows.Forms.TextBox();
            this.btnReload = new System.Windows.Forms.Button();
            this.btnThoat = new System.Windows.Forms.Button();
            this.btnXoa = new System.Windows.Forms.Button();
            this.btnLuu = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.gIAOVIEN_DANGKYDataGridView = new System.Windows.Forms.DataGridView();
            this.mAGVDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mAMHDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mALOPDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tRINHDODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nGAYTHIDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lANDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sOCAUTHIDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tHOIGIANDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gIAOVIEN_DANGKYTableAdapter = new THI_TDUNG.DSTableAdapters.GIAOVIEN_DANGKYTableAdapter();
            this.tableAdapterManager = new THI_TDUNG.DSTableAdapters.TableAdapterManager();
            this.lOPTableAdapter = new THI_TDUNG.DSTableAdapters.LOPTableAdapter();
            this.mONHOCTableAdapter = new THI_TDUNG.DSTableAdapters.MONHOCTableAdapter();
            mAGVLabel = new System.Windows.Forms.Label();
            mAMHLabel = new System.Windows.Forms.Label();
            mALOPLabel = new System.Windows.Forms.Label();
            tRINHDOLabel = new System.Windows.Forms.Label();
            nGAYTHILabel = new System.Windows.Forms.Label();
            lANLabel = new System.Windows.Forms.Label();
            sOCAUTHILabel = new System.Windows.Forms.Label();
            tHOIGIANLabel = new System.Windows.Forms.Label();
            this.grbNoiDung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAGV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gIAOVIEN_DANGKYBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mONHOCBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lOPBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGAYTHIDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGAYTHIDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speLan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gIAOVIEN_DANGKYDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // mAGVLabel
            // 
            mAGVLabel.AutoSize = true;
            mAGVLabel.Location = new System.Drawing.Point(51, 47);
            mAGVLabel.Name = "mAGVLabel";
            mAGVLabel.Size = new System.Drawing.Size(61, 20);
            mAGVLabel.TabIndex = 0;
            mAGVLabel.Text = "MAGV:";
            // 
            // mAMHLabel
            // 
            mAMHLabel.AutoSize = true;
            mAMHLabel.Location = new System.Drawing.Point(51, 84);
            mAMHLabel.Name = "mAMHLabel";
            mAMHLabel.Size = new System.Drawing.Size(62, 20);
            mAMHLabel.TabIndex = 2;
            mAMHLabel.Text = "MAMH:";
            // 
            // mALOPLabel
            // 
            mALOPLabel.AutoSize = true;
            mALOPLabel.Location = new System.Drawing.Point(51, 118);
            mALOPLabel.Name = "mALOPLabel";
            mALOPLabel.Size = new System.Drawing.Size(68, 20);
            mALOPLabel.TabIndex = 4;
            mALOPLabel.Text = "MALOP:";
            // 
            // tRINHDOLabel
            // 
            tRINHDOLabel.AutoSize = true;
            tRINHDOLabel.Location = new System.Drawing.Point(51, 152);
            tRINHDOLabel.Name = "tRINHDOLabel";
            tRINHDOLabel.Size = new System.Drawing.Size(86, 20);
            tRINHDOLabel.TabIndex = 6;
            tRINHDOLabel.Text = "TRINHDO:";
            // 
            // nGAYTHILabel
            // 
            nGAYTHILabel.AutoSize = true;
            nGAYTHILabel.Location = new System.Drawing.Point(320, 47);
            nGAYTHILabel.Name = "nGAYTHILabel";
            nGAYTHILabel.Size = new System.Drawing.Size(85, 20);
            nGAYTHILabel.TabIndex = 8;
            nGAYTHILabel.Text = "NGAYTHI:";
            // 
            // lANLabel
            // 
            lANLabel.AutoSize = true;
            lANLabel.Location = new System.Drawing.Point(320, 81);
            lANLabel.Name = "lANLabel";
            lANLabel.Size = new System.Drawing.Size(44, 20);
            lANLabel.TabIndex = 10;
            lANLabel.Text = "LAN:";
            // 
            // sOCAUTHILabel
            // 
            sOCAUTHILabel.AutoSize = true;
            sOCAUTHILabel.Location = new System.Drawing.Point(320, 118);
            sOCAUTHILabel.Name = "sOCAUTHILabel";
            sOCAUTHILabel.Size = new System.Drawing.Size(96, 20);
            sOCAUTHILabel.TabIndex = 12;
            sOCAUTHILabel.Text = "SOCAUTHI:";
            // 
            // tHOIGIANLabel
            // 
            tHOIGIANLabel.AutoSize = true;
            tHOIGIANLabel.Location = new System.Drawing.Point(320, 150);
            tHOIGIANLabel.Name = "tHOIGIANLabel";
            tHOIGIANLabel.Size = new System.Drawing.Size(91, 20);
            tHOIGIANLabel.TabIndex = 14;
            tHOIGIANLabel.Text = "THOIGIAN:";
            // 
            // btnUndo
            // 
            this.btnUndo.Location = new System.Drawing.Point(891, 441);
            this.btnUndo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(137, 36);
            this.btnUndo.TabIndex = 90;
            this.btnUndo.Text = "Undo";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // grbNoiDung
            // 
            this.grbNoiDung.Controls.Add(mAGVLabel);
            this.grbNoiDung.Controls.Add(this.txtMAGV);
            this.grbNoiDung.Controls.Add(mAMHLabel);
            this.grbNoiDung.Controls.Add(this.mAMHComboBox);
            this.grbNoiDung.Controls.Add(mALOPLabel);
            this.grbNoiDung.Controls.Add(this.mALOPComboBox);
            this.grbNoiDung.Controls.Add(tRINHDOLabel);
            this.grbNoiDung.Controls.Add(this.tRINHDOComboBox);
            this.grbNoiDung.Controls.Add(nGAYTHILabel);
            this.grbNoiDung.Controls.Add(this.nGAYTHIDateEdit);
            this.grbNoiDung.Controls.Add(lANLabel);
            this.grbNoiDung.Controls.Add(this.speLan);
            this.grbNoiDung.Controls.Add(sOCAUTHILabel);
            this.grbNoiDung.Controls.Add(this.sOCAUTHITextBox);
            this.grbNoiDung.Controls.Add(tHOIGIANLabel);
            this.grbNoiDung.Controls.Add(this.tHOIGIANTextBox);
            this.grbNoiDung.Location = new System.Drawing.Point(101, 322);
            this.grbNoiDung.Name = "grbNoiDung";
            this.grbNoiDung.Size = new System.Drawing.Size(602, 209);
            this.grbNoiDung.TabIndex = 89;
            this.grbNoiDung.TabStop = false;
            this.grbNoiDung.Text = "Nội Dung";
            // 
            // txtMAGV
            // 
            this.txtMAGV.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.gIAOVIEN_DANGKYBindingSource, "MAGV", true));
            this.txtMAGV.Location = new System.Drawing.Point(153, 44);
            this.txtMAGV.Name = "txtMAGV";
            this.txtMAGV.Properties.ReadOnly = true;
            this.txtMAGV.Size = new System.Drawing.Size(150, 28);
            this.txtMAGV.TabIndex = 1;
            // 
            // gIAOVIEN_DANGKYBindingSource
            // 
            this.gIAOVIEN_DANGKYBindingSource.DataMember = "GIAOVIEN_DANGKY";
            this.gIAOVIEN_DANGKYBindingSource.DataSource = this.dS;
            // 
            // dS
            // 
            this.dS.DataSetName = "DS";
            this.dS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // mAMHComboBox
            // 
            this.mAMHComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gIAOVIEN_DANGKYBindingSource, "MAMH", true));
            this.mAMHComboBox.DataSource = this.mONHOCBindingSource;
            this.mAMHComboBox.DisplayMember = "MAMH";
            this.mAMHComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mAMHComboBox.FormattingEnabled = true;
            this.mAMHComboBox.Location = new System.Drawing.Point(153, 81);
            this.mAMHComboBox.Name = "mAMHComboBox";
            this.mAMHComboBox.Size = new System.Drawing.Size(150, 28);
            this.mAMHComboBox.TabIndex = 3;
            this.mAMHComboBox.ValueMember = "MAMH";
            // 
            // mONHOCBindingSource
            // 
            this.mONHOCBindingSource.DataMember = "MONHOC";
            this.mONHOCBindingSource.DataSource = this.dS;
            // 
            // mALOPComboBox
            // 
            this.mALOPComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gIAOVIEN_DANGKYBindingSource, "MALOP", true));
            this.mALOPComboBox.DataSource = this.lOPBindingSource;
            this.mALOPComboBox.DisplayMember = "MALOP";
            this.mALOPComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mALOPComboBox.FormattingEnabled = true;
            this.mALOPComboBox.Location = new System.Drawing.Point(153, 115);
            this.mALOPComboBox.Name = "mALOPComboBox";
            this.mALOPComboBox.Size = new System.Drawing.Size(150, 28);
            this.mALOPComboBox.TabIndex = 5;
            this.mALOPComboBox.ValueMember = "MALOP";
            // 
            // lOPBindingSource
            // 
            this.lOPBindingSource.DataMember = "LOP";
            this.lOPBindingSource.DataSource = this.dS;
            // 
            // tRINHDOComboBox
            // 
            this.tRINHDOComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gIAOVIEN_DANGKYBindingSource, "TRINHDO", true));
            this.tRINHDOComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tRINHDOComboBox.FormattingEnabled = true;
            this.tRINHDOComboBox.Items.AddRange(new object[] {
            "A",
            "B",
            "C"});
            this.tRINHDOComboBox.Location = new System.Drawing.Point(153, 149);
            this.tRINHDOComboBox.Name = "tRINHDOComboBox";
            this.tRINHDOComboBox.Size = new System.Drawing.Size(150, 28);
            this.tRINHDOComboBox.TabIndex = 7;
            this.tRINHDOComboBox.SelectedIndexChanged += new System.EventHandler(this.tRINHDOComboBox_SelectedIndexChanged);
            // 
            // nGAYTHIDateEdit
            // 
            this.nGAYTHIDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.gIAOVIEN_DANGKYBindingSource, "NGAYTHI", true));
            this.nGAYTHIDateEdit.EditValue = null;
            this.nGAYTHIDateEdit.Location = new System.Drawing.Point(422, 44);
            this.nGAYTHIDateEdit.Name = "nGAYTHIDateEdit";
            this.nGAYTHIDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.nGAYTHIDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.nGAYTHIDateEdit.Size = new System.Drawing.Size(150, 28);
            this.nGAYTHIDateEdit.TabIndex = 9;
            // 
            // speLan
            // 
            this.speLan.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.gIAOVIEN_DANGKYBindingSource, "LAN", true));
            this.speLan.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.speLan.Location = new System.Drawing.Point(422, 78);
            this.speLan.Name = "speLan";
            this.speLan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speLan.Size = new System.Drawing.Size(150, 28);
            this.speLan.TabIndex = 11;
            // 
            // sOCAUTHITextBox
            // 
            this.sOCAUTHITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gIAOVIEN_DANGKYBindingSource, "SOCAUTHI", true));
            this.sOCAUTHITextBox.Location = new System.Drawing.Point(422, 115);
            this.sOCAUTHITextBox.Name = "sOCAUTHITextBox";
            this.sOCAUTHITextBox.Size = new System.Drawing.Size(150, 26);
            this.sOCAUTHITextBox.TabIndex = 13;
            // 
            // tHOIGIANTextBox
            // 
            this.tHOIGIANTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gIAOVIEN_DANGKYBindingSource, "THOIGIAN", true));
            this.tHOIGIANTextBox.Location = new System.Drawing.Point(422, 147);
            this.tHOIGIANTextBox.Name = "tHOIGIANTextBox";
            this.tHOIGIANTextBox.Size = new System.Drawing.Size(150, 26);
            this.tHOIGIANTextBox.TabIndex = 15;
            // 
            // btnReload
            // 
            this.btnReload.Location = new System.Drawing.Point(891, 377);
            this.btnReload.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(137, 36);
            this.btnReload.TabIndex = 88;
            this.btnReload.Text = "Reload";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(891, 322);
            this.btnThoat.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(137, 36);
            this.btnThoat.TabIndex = 87;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(726, 440);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(137, 36);
            this.btnXoa.TabIndex = 86;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(726, 380);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(137, 36);
            this.btnLuu.TabIndex = 85;
            this.btnLuu.Text = "Ghi";
            this.btnLuu.UseVisualStyleBackColor = true;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(726, 322);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(137, 36);
            this.btnThem.TabIndex = 84;
            this.btnThem.Text = "Thêm";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // gIAOVIEN_DANGKYDataGridView
            // 
            this.gIAOVIEN_DANGKYDataGridView.AutoGenerateColumns = false;
            this.gIAOVIEN_DANGKYDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gIAOVIEN_DANGKYDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gIAOVIEN_DANGKYDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mAGVDataGridViewTextBoxColumn,
            this.mAMHDataGridViewTextBoxColumn,
            this.mALOPDataGridViewTextBoxColumn,
            this.tRINHDODataGridViewTextBoxColumn,
            this.nGAYTHIDataGridViewTextBoxColumn,
            this.lANDataGridViewTextBoxColumn,
            this.sOCAUTHIDataGridViewTextBoxColumn,
            this.tHOIGIANDataGridViewTextBoxColumn});
            this.gIAOVIEN_DANGKYDataGridView.DataSource = this.gIAOVIEN_DANGKYBindingSource;
            this.gIAOVIEN_DANGKYDataGridView.Location = new System.Drawing.Point(88, 63);
            this.gIAOVIEN_DANGKYDataGridView.Name = "gIAOVIEN_DANGKYDataGridView";
            this.gIAOVIEN_DANGKYDataGridView.RowTemplate.Height = 28;
            this.gIAOVIEN_DANGKYDataGridView.Size = new System.Drawing.Size(956, 240);
            this.gIAOVIEN_DANGKYDataGridView.TabIndex = 83;
            // 
            // mAGVDataGridViewTextBoxColumn
            // 
            this.mAGVDataGridViewTextBoxColumn.DataPropertyName = "MAGV";
            this.mAGVDataGridViewTextBoxColumn.HeaderText = "MAGV";
            this.mAGVDataGridViewTextBoxColumn.Name = "mAGVDataGridViewTextBoxColumn";
            // 
            // mAMHDataGridViewTextBoxColumn
            // 
            this.mAMHDataGridViewTextBoxColumn.DataPropertyName = "MAMH";
            this.mAMHDataGridViewTextBoxColumn.HeaderText = "MAMH";
            this.mAMHDataGridViewTextBoxColumn.Name = "mAMHDataGridViewTextBoxColumn";
            // 
            // mALOPDataGridViewTextBoxColumn
            // 
            this.mALOPDataGridViewTextBoxColumn.DataPropertyName = "MALOP";
            this.mALOPDataGridViewTextBoxColumn.HeaderText = "MALOP";
            this.mALOPDataGridViewTextBoxColumn.Name = "mALOPDataGridViewTextBoxColumn";
            // 
            // tRINHDODataGridViewTextBoxColumn
            // 
            this.tRINHDODataGridViewTextBoxColumn.DataPropertyName = "TRINHDO";
            this.tRINHDODataGridViewTextBoxColumn.HeaderText = "TRINHDO";
            this.tRINHDODataGridViewTextBoxColumn.Name = "tRINHDODataGridViewTextBoxColumn";
            // 
            // nGAYTHIDataGridViewTextBoxColumn
            // 
            this.nGAYTHIDataGridViewTextBoxColumn.DataPropertyName = "NGAYTHI";
            this.nGAYTHIDataGridViewTextBoxColumn.HeaderText = "NGAYTHI";
            this.nGAYTHIDataGridViewTextBoxColumn.Name = "nGAYTHIDataGridViewTextBoxColumn";
            // 
            // lANDataGridViewTextBoxColumn
            // 
            this.lANDataGridViewTextBoxColumn.DataPropertyName = "LAN";
            this.lANDataGridViewTextBoxColumn.HeaderText = "LAN";
            this.lANDataGridViewTextBoxColumn.Name = "lANDataGridViewTextBoxColumn";
            // 
            // sOCAUTHIDataGridViewTextBoxColumn
            // 
            this.sOCAUTHIDataGridViewTextBoxColumn.DataPropertyName = "SOCAUTHI";
            this.sOCAUTHIDataGridViewTextBoxColumn.HeaderText = "SOCAUTHI";
            this.sOCAUTHIDataGridViewTextBoxColumn.Name = "sOCAUTHIDataGridViewTextBoxColumn";
            // 
            // tHOIGIANDataGridViewTextBoxColumn
            // 
            this.tHOIGIANDataGridViewTextBoxColumn.DataPropertyName = "THOIGIAN";
            this.tHOIGIANDataGridViewTextBoxColumn.HeaderText = "THOIGIAN";
            this.tHOIGIANDataGridViewTextBoxColumn.Name = "tHOIGIANDataGridViewTextBoxColumn";
            // 
            // gIAOVIEN_DANGKYTableAdapter
            // 
            this.gIAOVIEN_DANGKYTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BANGDIEMTableAdapter = null;
            this.tableAdapterManager.BODETableAdapter = null;
            this.tableAdapterManager.COSOTableAdapter = null;
            this.tableAdapterManager.GIAOVIEN_DANGKYTableAdapter = this.gIAOVIEN_DANGKYTableAdapter;
            this.tableAdapterManager.GIAOVIENTableAdapter = null;
            this.tableAdapterManager.KHOATableAdapter = null;
            this.tableAdapterManager.LOPTableAdapter = this.lOPTableAdapter;
            this.tableAdapterManager.MONHOCTableAdapter = this.mONHOCTableAdapter;
            this.tableAdapterManager.SINHVIENTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = THI_TDUNG.DSTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // lOPTableAdapter
            // 
            this.lOPTableAdapter.ClearBeforeFill = true;
            // 
            // mONHOCTableAdapter
            // 
            this.mONHOCTableAdapter.ClearBeforeFill = true;
            // 
            // frmDkThi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1133, 558);
            this.Controls.Add(this.btnUndo);
            this.Controls.Add(this.grbNoiDung);
            this.Controls.Add(this.btnReload);
            this.Controls.Add(this.btnThoat);
            this.Controls.Add(this.btnXoa);
            this.Controls.Add(this.btnLuu);
            this.Controls.Add(this.btnThem);
            this.Controls.Add(this.gIAOVIEN_DANGKYDataGridView);
            this.Name = "frmDkThi";
            this.Text = "frmDkThi";
            this.Load += new System.EventHandler(this.frmDkThi_Load);
            this.grbNoiDung.ResumeLayout(false);
            this.grbNoiDung.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAGV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gIAOVIEN_DANGKYBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mONHOCBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lOPBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGAYTHIDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nGAYTHIDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speLan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gIAOVIEN_DANGKYDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.GroupBox grbNoiDung;
        private System.Windows.Forms.Button btnReload;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.Button btnLuu;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.DataGridView gIAOVIEN_DANGKYDataGridView;
        private DS dS;
        private System.Windows.Forms.BindingSource gIAOVIEN_DANGKYBindingSource;
        private DSTableAdapters.GIAOVIEN_DANGKYTableAdapter gIAOVIEN_DANGKYTableAdapter;
        private DSTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraEditors.TextEdit txtMAGV;
        private System.Windows.Forms.ComboBox mAMHComboBox;
        private System.Windows.Forms.ComboBox mALOPComboBox;
        private System.Windows.Forms.ComboBox tRINHDOComboBox;
        private DevExpress.XtraEditors.DateEdit nGAYTHIDateEdit;
        private DevExpress.XtraEditors.SpinEdit speLan;
        private System.Windows.Forms.TextBox sOCAUTHITextBox;
        private System.Windows.Forms.TextBox tHOIGIANTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn mAGVDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mAMHDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mALOPDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tRINHDODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nGAYTHIDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lANDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sOCAUTHIDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tHOIGIANDataGridViewTextBoxColumn;
        private DSTableAdapters.MONHOCTableAdapter mONHOCTableAdapter;
        private System.Windows.Forms.BindingSource mONHOCBindingSource;
        private DSTableAdapters.LOPTableAdapter lOPTableAdapter;
        private System.Windows.Forms.BindingSource lOPBindingSource;
    }
}