﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace THI_TDUNG
{
    public partial class frmDkThi : Form
    {
        private int vitri;
        private Stack<UndoStack> undoTarget = new Stack<UndoStack>();
        class GV_DK
        {
            public string MAGV, MAMH, MALOP, TRINHDO;
            public DateTime NGAYTHI;
            public short LAN, SOCAUTHI, THOIGIAN;
            public GV_DK(string MAGV, string MAMH, string MALOP, string TRINHDO, DateTime NGAYTHI, short LAN, short SOCAUTHI, short THOIGIAN)
            {
                this.MAGV = MAGV;
                this.MAMH = MAMH;
                this.MALOP = MALOP;
                this.TRINHDO = TRINHDO;
                this.NGAYTHI = NGAYTHI;
                this.LAN = LAN;
                this.SOCAUTHI = SOCAUTHI;
                this.THOIGIAN = THOIGIAN;
            }
        }
        void pushGV_DK(GV_DK data,String action)
        {
            UndoStack target = new UndoStack(data, action);
            this.undoTarget.Push(target);
        }

        UndoStack popGV_DK()
        {
            return this.undoTarget.Pop();
        }
        void showTop()
        {
            UndoStack dataTop = this.undoTarget.Peek();
            MessageBox.Show(
                dataTop.HANHDONG+":"+
                dataTop.data.MAGV +":"+
                dataTop.data.MAMH +":"+
                dataTop.data.MALOP+":"+
                dataTop.data.TRINHDO+":"+
                dataTop.data.NGAYTHI+":"+
                dataTop.data.LAN.ToString()+":"+
                dataTop.data.SOCAUTHI.ToString() + ":" +
                dataTop.data.THOIGIAN.ToString() + ":");
        }

        public frmDkThi()
        {
            InitializeComponent();
        }

        private void frmDkThi_Load(object sender, EventArgs e)
        {
            dS.EnforceConstraints = false;
            // TODO: This line of code loads data into the 'dS.LOP' table. You can move, or remove it, as needed.
            this.lOPTableAdapter.Connection.ConnectionString = Program.connstr;
            this.lOPTableAdapter.Fill(this.dS.LOP);
            // TODO: This line of code loads data into the 'dS.MONHOC' table. You can move, or remove it, as needed.
            this.mONHOCTableAdapter.Connection.ConnectionString = Program.connstr;
            this.mONHOCTableAdapter.Fill(this.dS.MONHOC);
            // TODO: This line of code loads data into the 'dS.GIAOVIEN_DANGKY' table. You can move, or remove it, as needed.
            this.gIAOVIEN_DANGKYTableAdapter.Connection.ConnectionString = Program.connstr;
            this.gIAOVIEN_DANGKYTableAdapter.Fill(this.dS.GIAOVIEN_DANGKY);
            if (Program.mGroup == "TRUONG")
            {
                MessageBox.Show("Bạn chỉ có quyền xem dữ liệu này!");
                btnThem.Enabled = btnLuu.Enabled = btnXoa.Enabled = btnUndo.Enabled = false;
                grbNoiDung.Enabled = false;
            }
            grbNoiDung.Enabled = false;
        }
        private bool check_giaoviendki(string mamh, string malop, int lan)
        {
            string strLenh = string.Format("SP_KT_GVDK '{0}','{1}',{2}", mamh,malop,lan);
            //isExist
            bool exist = false;
            using (SqlConnection connection = new SqlConnection(Program.connstr))
            {
                connection.Open();
                SqlCommand sqlcmd = new SqlCommand(strLenh, connection);
                sqlcmd.CommandType = CommandType.Text;
                try
                {
                    SqlDataReader myreader = sqlcmd.ExecuteReader();
                    exist = true;
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);
                    //MessageBox.Show("",e.Errors.ToString());
                    exist = false;
                }
            }
            return exist;
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            if (!grbNoiDung.Enabled)
            {
                grbNoiDung.Enabled = true;
                gIAOVIEN_DANGKYBindingSource.AddNew();
                txtMAGV.Text = Program.username;
                return;
            }
            try
            {
                if(check_giaoviendki(mAMHComboBox.Text.ToString().Trim(), mALOPComboBox.Text.ToString().Trim(), Convert.ToInt16(speLan.Text.ToString().Trim())))
                {
                vitri = gIAOVIEN_DANGKYBindingSource.Position;
                this.gIAOVIEN_DANGKYTableAdapter.Insert(
                        txtMAGV.Text.ToString().Trim(),
                        mAMHComboBox.Text.ToString().Trim(),
                        mALOPComboBox.Text.ToString().Trim(),
                        tRINHDOComboBox.Text.ToString().Trim(),
                        DateTime.Parse(nGAYTHIDateEdit.Text.ToString().Trim()),
                        Convert.ToInt16(speLan.Text.ToString().Trim()),
                        Convert.ToInt16(sOCAUTHITextBox.Text.ToString().Trim()),
                        Convert.ToInt16(tHOIGIANTextBox.Text.ToString().Trim())
                    );
                //Push them
                //DataRowView items = (DataRowView)this.gIAOVIEN_DANGKYBindingSource.List[vitri];
                if (true)
                {
                    GV_DK data = new GV_DK(
                       txtMAGV.Text.ToString().Trim(),
                       mAMHComboBox.Text.ToString().Trim(),
                       mALOPComboBox.Text.ToString().Trim(),
                       tRINHDOComboBox.Text.ToString().Trim(),
                       DateTime.Parse(nGAYTHIDateEdit.Text.ToString().Trim()),
                       Convert.ToInt16(speLan.Text.ToString().Trim()),
                       Convert.ToInt16(sOCAUTHITextBox.Text.ToString().Trim()),
                       Convert.ToInt16(tHOIGIANTextBox.Text.ToString().Trim())
                    );
                    pushGV_DK(data, "insert");
                    showTop();
                    grbNoiDung.Enabled = false;
                    MessageBox.Show("Thêm thành công");
                    }
                }
            }
            catch
            {
                MessageBox.Show("kHÔNG THÊM ĐC");
                gIAOVIEN_DANGKYBindingSource.ResetCurrentItem();
                grbNoiDung.Enabled = false;
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!grbNoiDung.Enabled)
            {
                grbNoiDung.Enabled = true;
                txtMAGV.Text = Program.username;
                vitri = gIAOVIEN_DANGKYBindingSource.Position;
                DataRowView items = (DataRowView)this.gIAOVIEN_DANGKYBindingSource.List[vitri];
                GV_DK data0 = new GV_DK(
                   items["MAGV"].ToString().Trim(),
                   items["MAMH"].ToString().Trim(),
                   items["MALOP"].ToString().Trim(),
                   items["TRINHDO"].ToString().Trim(),
                   DateTime.Parse(items["NGAYTHI"].ToString().Trim()),
                   Convert.ToInt16(items["LAN"].ToString().Trim()),
                   Convert.ToInt16(items["SOCAUTHI"].ToString().Trim()),
                   Convert.ToInt16(items["THOIGIAN"].ToString().Trim())
                );
                pushGV_DK(data0, "update0");
                return;
            }
            if (((DataRowView)gIAOVIEN_DANGKYBindingSource[gIAOVIEN_DANGKYBindingSource.Position])["MAGV"].ToString().Trim().CompareTo(Program.username.Trim()) != 0)
            {
                MessageBox.Show("Bạn không thể sửa câu hỏi của người khác", "", MessageBoxButtons.OK);
                return;
            }
            else
            {
                try
                {
                    

                    gIAOVIEN_DANGKYBindingSource.EndEdit();
                    gIAOVIEN_DANGKYBindingSource.ResetCurrentItem();
                    this.gIAOVIEN_DANGKYTableAdapter.Update(this.dS.GIAOVIEN_DANGKY);
                    
                    //Push them update

                    GV_DK data = new GV_DK(
                           txtMAGV.Text.ToString().Trim(),
                           mAMHComboBox.Text.ToString().Trim(),
                           mALOPComboBox.Text.ToString().Trim(),
                           tRINHDOComboBox.Text.ToString().Trim(),
                           DateTime.Parse(nGAYTHIDateEdit.Text.ToString().Trim()),
                           Convert.ToInt16(speLan.Text.ToString().Trim()),
                           Convert.ToInt16(sOCAUTHITextBox.Text.ToString().Trim()),
                           Convert.ToInt16(tHOIGIANTextBox.Text.ToString().Trim())
                        );
                    pushGV_DK(data, "update");
                    showTop();
                    grbNoiDung.Enabled = false;
                    MessageBox.Show("Sửa thành công", "", MessageBoxButtons.OK);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn thật sự có muốn xóa???", "Xác nhận", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                if (((DataRowView)gIAOVIEN_DANGKYBindingSource[gIAOVIEN_DANGKYBindingSource.Position])["MAGV"].ToString().Trim().CompareTo(Program.username.Trim()) != 0)
                {
                    MessageBox.Show("Bạn không thể xóa câu hỏi của người khác", "", MessageBoxButtons.OK);
                    return;
                }
                else
                {
                    try
                    {
                        vitri = gIAOVIEN_DANGKYBindingSource.Position;
                        DataRowView items = (DataRowView)this.gIAOVIEN_DANGKYBindingSource.List[vitri];
                        GV_DK data = new GV_DK(
                           items["MAGV"].ToString().Trim(),
                           items["MAMH"].ToString().Trim(),
                           items["MALOP"].ToString().Trim(),
                           items["TRINHDO"].ToString().Trim(),
                           DateTime.Parse(items["NGAYTHI"].ToString().Trim()),
                           Convert.ToInt16(items["LAN"].ToString().Trim()),
                           Convert.ToInt16(items["SOCAUTHI"].ToString().Trim()),
                           Convert.ToInt16(items["THOIGIAN"].ToString().Trim())
                        );
                        pushGV_DK(data, "delete");
                        showTop();
                        gIAOVIEN_DANGKYBindingSource.RemoveCurrent();
                        this.gIAOVIEN_DANGKYTableAdapter.Update(this.dS.GIAOVIEN_DANGKY);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        MessageBox.Show("Lỗi xóa", string.Empty, MessageBoxButtons.OK);
                    }
                }

            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            this.gIAOVIEN_DANGKYTableAdapter.Fill(this.dS.GIAOVIEN_DANGKY);
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            if (this.undoTarget.Count <= 0) return;
            UndoStack popData = popGV_DK();
            string action = popData.HANHDONG;
            switch (action)
            {
                case "insert":
                    int lenght = this.gIAOVIEN_DANGKYBindingSource.List.Count;
                    for(int i=0;i<= lenght ; i++)
                    {
                        DataRowView items = (DataRowView)this.gIAOVIEN_DANGKYBindingSource.List[i];
                        bool isMAGVTrue = items["MAGV"].ToString().Trim().CompareTo(popData.data.MAGV) == 0;
                        bool isMAMHTrue = items["MAMH"].ToString().Trim().CompareTo(popData.data.MAMH) == 0;
                        bool isMALOPTrue = items["MALOP"].ToString().Trim().CompareTo(popData.data.MALOP) == 0;
                        bool isLANTrue = Convert.ToInt16(items["LAN"].ToString().Trim()) == popData.data.LAN;
                        if(isLANTrue && isMAMHTrue &&isMALOPTrue && isLANTrue)
                        {
                            this.gIAOVIEN_DANGKYBindingSource.RemoveAt(i);
                            this.gIAOVIEN_DANGKYTableAdapter.Update(this.dS.GIAOVIEN_DANGKY);
                            break;
                        }
                    }
                    break;
                case "delete":
                    this.gIAOVIEN_DANGKYTableAdapter.Insert(
                        popData.data.MAGV,
                        popData.data.MAMH,
                        popData.data.MALOP,
                        popData.data.TRINHDO,
                        popData.data.NGAYTHI,
                        popData.data.LAN,
                        popData.data.SOCAUTHI,
                        popData.data.THOIGIAN
                        );
                    break;
                case "update":
                    this.showTop();
                    UndoStack popData0 = popGV_DK();
                    this.gIAOVIEN_DANGKYTableAdapter.Update(
                        popData0.data.MAGV,
                        popData0.data.MAMH,
                        popData0.data.MALOP,
                        popData0.data.TRINHDO,
                        popData0.data.NGAYTHI,
                        popData0.data.LAN,
                        popData0.data.SOCAUTHI,
                        popData0.data.THOIGIAN,
                        popData.data.MAGV,
                        popData.data.MAMH,
                        popData.data.MALOP,
                        popData.data.TRINHDO,
                        popData.data.NGAYTHI,
                        popData.data.LAN,
                        popData.data.SOCAUTHI,
                        popData.data.THOIGIAN
                        );
                    break;
                default:
                    break;
            }

        }

        private void tRINHDOComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
