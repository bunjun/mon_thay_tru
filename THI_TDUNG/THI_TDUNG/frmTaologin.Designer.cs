﻿namespace THI_TDUNG
{
    partial class frmTaologin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtXacnhan = new System.Windows.Forms.TextBox();
            this.radTruong = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.radGiangVien = new System.Windows.Forms.RadioButton();
            this.btnThoat = new System.Windows.Forms.Button();
            this.radCoso = new System.Windows.Forms.RadioButton();
            this.btnTaologin = new System.Windows.Forms.Button();
            this.cmbMAGV = new System.Windows.Forms.ComboBox();
            this.gIAOVIENBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dS = new THI_TDUNG.DS();
            this.txtMATKHAU = new System.Windows.Forms.TextBox();
            this.txtTAIKHOAN = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gIAOVIENTableAdapter = new THI_TDUNG.DSTableAdapters.GIAOVIENTableAdapter();
            this.tableAdapterManager = new THI_TDUNG.DSTableAdapters.TableAdapterManager();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gIAOVIENBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtXacnhan);
            this.groupBox1.Controls.Add(this.radTruong);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.radGiangVien);
            this.groupBox1.Controls.Add(this.btnThoat);
            this.groupBox1.Controls.Add(this.radCoso);
            this.groupBox1.Controls.Add(this.btnTaologin);
            this.groupBox1.Controls.Add(this.cmbMAGV);
            this.groupBox1.Controls.Add(this.txtMATKHAU);
            this.groupBox1.Controls.Add(this.txtTAIKHOAN);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(33, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(749, 568);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TẠO TÀI KHOẢN";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(96, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 30);
            this.label4.TabIndex = 16;
            this.label4.Text = "QUYỀN";
            // 
            // txtXacnhan
            // 
            this.txtXacnhan.Location = new System.Drawing.Point(341, 345);
            this.txtXacnhan.Multiline = true;
            this.txtXacnhan.Name = "txtXacnhan";
            this.txtXacnhan.Size = new System.Drawing.Size(336, 41);
            this.txtXacnhan.TabIndex = 15;
            // 
            // radTruong
            // 
            this.radTruong.AutoSize = true;
            this.radTruong.Location = new System.Drawing.Point(341, 49);
            this.radTruong.Name = "radTruong";
            this.radTruong.Size = new System.Drawing.Size(103, 24);
            this.radTruong.TabIndex = 11;
            this.radTruong.TabStop = true;
            this.radTruong.Text = "TRƯỜNG";
            this.radTruong.UseVisualStyleBackColor = true;
            this.radTruong.CheckedChanged += new System.EventHandler(this.radTruong_CheckedChanged_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(47, 345);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(288, 30);
            this.label5.TabIndex = 14;
            this.label5.Text = "XÁC NHẬN MẬT KHẨU";
            // 
            // radGiangVien
            // 
            this.radGiangVien.AutoSize = true;
            this.radGiangVien.Location = new System.Drawing.Point(341, 128);
            this.radGiangVien.Name = "radGiangVien";
            this.radGiangVien.Size = new System.Drawing.Size(129, 24);
            this.radGiangVien.TabIndex = 13;
            this.radGiangVien.TabStop = true;
            this.radGiangVien.Text = "GIẢNG VIÊN";
            this.radGiangVien.UseVisualStyleBackColor = true;
            this.radGiangVien.CheckedChanged += new System.EventHandler(this.radGiangVien_CheckedChanged_1);
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(426, 492);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(116, 49);
            this.btnThoat.TabIndex = 9;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click_1);
            // 
            // radCoso
            // 
            this.radCoso.AutoSize = true;
            this.radCoso.Location = new System.Drawing.Point(341, 88);
            this.radCoso.Name = "radCoso";
            this.radCoso.Size = new System.Drawing.Size(84, 24);
            this.radCoso.TabIndex = 12;
            this.radCoso.TabStop = true;
            this.radCoso.Text = "CƠ SỞ";
            this.radCoso.UseVisualStyleBackColor = true;
            this.radCoso.CheckedChanged += new System.EventHandler(this.radCoso_CheckedChanged_1);
            // 
            // btnTaologin
            // 
            this.btnTaologin.Location = new System.Drawing.Point(231, 492);
            this.btnTaologin.Name = "btnTaologin";
            this.btnTaologin.Size = new System.Drawing.Size(116, 49);
            this.btnTaologin.TabIndex = 1;
            this.btnTaologin.Text = "Tạo tài khoản";
            this.btnTaologin.UseVisualStyleBackColor = true;
            this.btnTaologin.Click += new System.EventHandler(this.btnTaologin_Click_1);
            // 
            // cmbMAGV
            // 
            this.cmbMAGV.DataSource = this.gIAOVIENBindingSource;
            this.cmbMAGV.DisplayMember = "MAGV";
            this.cmbMAGV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMAGV.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMAGV.FormattingEnabled = true;
            this.cmbMAGV.Location = new System.Drawing.Point(341, 421);
            this.cmbMAGV.Name = "cmbMAGV";
            this.cmbMAGV.Size = new System.Drawing.Size(336, 38);
            this.cmbMAGV.TabIndex = 7;
            this.cmbMAGV.ValueMember = "MAGV";
            // 
            // gIAOVIENBindingSource
            // 
            this.gIAOVIENBindingSource.DataMember = "GIAOVIEN";
            this.gIAOVIENBindingSource.DataSource = this.dS;
            // 
            // dS
            // 
            this.dS.DataSetName = "DS";
            this.dS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // txtMATKHAU
            // 
            this.txtMATKHAU.Location = new System.Drawing.Point(341, 267);
            this.txtMATKHAU.Multiline = true;
            this.txtMATKHAU.Name = "txtMATKHAU";
            this.txtMATKHAU.Size = new System.Drawing.Size(336, 41);
            this.txtMATKHAU.TabIndex = 4;
            // 
            // txtTAIKHOAN
            // 
            this.txtTAIKHOAN.Location = new System.Drawing.Point(341, 205);
            this.txtTAIKHOAN.Multiline = true;
            this.txtTAIKHOAN.Name = "txtTAIKHOAN";
            this.txtTAIKHOAN.Size = new System.Drawing.Size(336, 41);
            this.txtTAIKHOAN.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(47, 424);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 30);
            this.label3.TabIndex = 2;
            this.label3.Text = "USERNAME";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(47, 278);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 30);
            this.label2.TabIndex = 1;
            this.label2.Text = "MẬT KHẨU";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(47, 216);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(210, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "TÊN TÀI KHOẢN";
            // 
            // gIAOVIENTableAdapter
            // 
            this.gIAOVIENTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BANGDIEMTableAdapter = null;
            this.tableAdapterManager.BODETableAdapter = null;
            this.tableAdapterManager.COSOTableAdapter = null;
            this.tableAdapterManager.GIAOVIEN_DANGKYTableAdapter = null;
            this.tableAdapterManager.GIAOVIENTableAdapter = this.gIAOVIENTableAdapter;
            this.tableAdapterManager.KHOATableAdapter = null;
            this.tableAdapterManager.LOPTableAdapter = null;
            this.tableAdapterManager.MONHOCTableAdapter = null;
            this.tableAdapterManager.SINHVIENTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = THI_TDUNG.DSTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // frmTaologin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 831);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmTaologin";
            this.Text = "frmTaologin";
            this.Load += new System.EventHandler(this.frmTaologin_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gIAOVIENBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtXacnhan;
        private System.Windows.Forms.RadioButton radTruong;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton radGiangVien;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.RadioButton radCoso;
        private System.Windows.Forms.Button btnTaologin;
        private System.Windows.Forms.ComboBox cmbMAGV;
        private System.Windows.Forms.TextBox txtMATKHAU;
        private System.Windows.Forms.TextBox txtTAIKHOAN;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DS dS;
        private System.Windows.Forms.BindingSource gIAOVIENBindingSource;
        private DSTableAdapters.GIAOVIENTableAdapter gIAOVIENTableAdapter;
        private DSTableAdapters.TableAdapterManager tableAdapterManager;
    }
}