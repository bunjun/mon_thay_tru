﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace THI_TDUNG
{
    public partial class RP_INBDMH : DevExpress.XtraReports.UI.XtraReport
    {
        public RP_INBDMH(String malop, String mamh, int lan)
        {
            InitializeComponent();
            ds1.EnforceConstraints = false;
            this.sp_InbdmhTableAdapter1.Connection.ConnectionString = Program.connstr;
            this.sp_InbdmhTableAdapter1.sp_Inbdmh(ds1.sp_Inbdmh, malop, mamh, lan);
        }

    }
}
