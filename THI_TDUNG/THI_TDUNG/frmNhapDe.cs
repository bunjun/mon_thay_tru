﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace THI_TDUNG
{
    public partial class frmNhapDe : Form
    {
        private int vitri;
        private string magv = Program.username;
        private static bool isCreated = false;

        private Stack<UndoStack> undoTarget = new Stack<UndoStack>();
        class BODE
        {
            public short CAUHOI;
            public string MAMH, TRINHDO, NOIDUNG, A, B, C, D, DAP_AN, MAGV;
            public BODE(
                    short CAUHOI,
                    string MAMH,
                    string TRINHDO,
                    string NOIDUNG,
                    string A,
                    string B,
                    string C,
                    string D,
                    string DAP_AN,
                    string MAGV
                )
            {
                this.CAUHOI = CAUHOI;
                this.MAMH = MAMH;
                this.TRINHDO = TRINHDO;
                this.NOIDUNG = NOIDUNG;
                this.A = A;
                this.B = B;
                this.C = C;
                this.D = D;
                this.DAP_AN = DAP_AN;
                this.MAGV = MAGV;
            }
        }
        void pushBODE(BODE data, String action)
        {
            UndoStack target = new UndoStack(data, action);
            this.undoTarget.Push(target);
        }

        UndoStack popBODE()
        {
            return this.undoTarget.Pop();
        }
        void showTop()
        {
            UndoStack dataTop = this.undoTarget.Peek();
            MessageBox.Show(
                dataTop.HANHDONG + ":" +
                dataTop.data.MAMH + ":" +
                dataTop.data.TRINHDO + ":" +
                dataTop.data.NOIDUNG + ":");
        }

        public frmNhapDe()
        {
            InitializeComponent();
        }

        private void frmNhapDe_Load(object sender, EventArgs e)
        {
            dS.EnforceConstraints = false;
            isCreated = false;
            // TODO: This line of code loads data into the 'dS.MONHOC' table. You can move, or remove it, as needed.
            this.mONHOCTableAdapter.Fill(this.dS.MONHOC);
            // TODO: This line of code loads data into the 'dS.BODE' table. You can move, or remove it, as needed.
            this.bODETableAdapter.Fill(this.dS.BODE);
            if (Program.mGroup == "TRUONG")
            {
                MessageBox.Show("Bạn chỉ có quyền xem dữ liệu này!");
                btnThem.Enabled = btnGhi.Enabled = btnXoa.Enabled = btnUndo.Enabled = false;
                grbNoiDung.Enabled = false;
            }
            grbNoiDung.Enabled = false;

        }

        private void dAP_ANComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tRINHDOComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private bool check_thembode(int macauhoi)
        {
            string strLenh = string.Format("SP_KT_MA_BO_DE {0}", macauhoi);
            //isExist
            bool exist = false;
            using (SqlConnection connection = new SqlConnection(Program.connstr))
            {
                connection.Open();
                SqlCommand sqlcmd = new SqlCommand(strLenh, connection);
                sqlcmd.CommandType = CommandType.Text;
                try
                {
                    SqlDataReader myreader = sqlcmd.ExecuteReader();
                    exist = true;
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);
                    //MessageBox.Show("",e.Errors.ToString());
                    exist = false;
                }
            }
            return exist;
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            if (!grbNoiDung.Enabled)
            {
                grbNoiDung.Enabled = true;
                bODEBindingSource.AddNew();
                txtMaGV.Text = Program.username;
                return;
            }
            try
            {
                if (check_thembode(Convert.ToInt16(speCauhoi.Text.ToString().Trim())) == true)
                {


                    vitri = bODEBindingSource.Position;
                    this.bODETableAdapter.Insert(
                            Convert.ToInt16(speCauhoi.Text.ToString().Trim()),
                            cmbMaMH.Text.ToString().Trim(),
                            cmbTrinhDo.Text.ToString().Trim(),
                            nOIDUNGTextEdit.Text.ToString().Trim(),
                            aTextEdit.Text.ToString().Trim(),
                            bTextEdit.Text.ToString().Trim(),
                            cTextEdit.Text.ToString().Trim(),
                            dTextEdit.Text.ToString().Trim(),
                            cmbDapAn.Text.ToString().Trim(),
                            txtMaGV.Text.ToString().Trim()
                        );
                    //Push them
                    //DataRowView items = (DataRowView)this.gIAOVIEN_DANGKYBindingSource.List[vitri];
                    if (true)
                    {
                        BODE data = new BODE(
                               Convert.ToInt16(speCauhoi.Text.ToString().Trim()),
                               cmbMaMH.Text.ToString().Trim(),
                               cmbTrinhDo.Text.ToString().Trim(),
                               nOIDUNGTextEdit.Text.ToString().Trim(),
                               aTextEdit.Text.ToString().Trim(),
                               bTextEdit.Text.ToString().Trim(),
                               cTextEdit.Text.ToString().Trim(),
                               dTextEdit.Text.ToString().Trim(),
                               cmbDapAn.Text.ToString().Trim(),
                               txtMaGV.Text.ToString().Trim()
                        );
                        pushBODE(data, "insert");
                        showTop();
                        grbNoiDung.Enabled = false;
                        MessageBox.Show("Thêm thành công");
                    }
                }
            }
            catch
            {
                MessageBox.Show("kHÔNG THÊM ĐC");
                bODEBindingSource.ResetCurrentItem();
                grbNoiDung.Enabled = false;
            }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            
            if (!grbNoiDung.Enabled)
            {
                grbNoiDung.Enabled = true;
                txtMaGV.Text = Program.username;
                vitri = bODEBindingSource.Position;
                DataRowView items = (DataRowView)this.bODEBindingSource.List[vitri];
                BODE data0 = new BODE(
                   Convert.ToInt16(items["CAUHOI"].ToString().Trim()),
                   items["MAMH"].ToString().Trim(),
                   items["TRINHDO"].ToString().Trim(),
                   items["NOIDUNG"].ToString().Trim(),
                   items["A"].ToString().Trim(),
                   items["B"].ToString().Trim(),
                   items["C"].ToString().Trim(),
                   items["D"].ToString().Trim(),
                   items["DAP_AN"].ToString().Trim(),
                   items["MAGV"].ToString().Trim()
                );
                pushBODE(data0, "update0");
                return;
            }
            if (((DataRowView)bODEBindingSource[bODEBindingSource.Position])["MAGV"].ToString().Trim().CompareTo(Program.username.Trim()) != 0)
            {
                MessageBox.Show("Bạn không thể sửa câu hỏi của người khác", "", MessageBoxButtons.OK);
                return;
            }
            else
            {
                try
                {
                    bODEBindingSource.EndEdit();
                    bODEBindingSource.ResetCurrentItem();
                    this.bODETableAdapter.Update(this.dS.BODE);
                    BODE data = new BODE(
                      Convert.ToInt16(speCauhoi.Text.ToString().Trim()),
                       cmbMaMH.Text.ToString().Trim(),
                       cmbTrinhDo.Text.ToString().Trim(),
                       nOIDUNGTextEdit.Text.ToString().Trim(),
                       aTextEdit.Text.ToString().Trim(),
                       bTextEdit.Text.ToString().Trim(),
                       cTextEdit.Text.ToString().Trim(),
                       dTextEdit.Text.ToString().Trim(),
                       cmbDapAn.Text.ToString().Trim(),
                       txtMaGV.Text.ToString().Trim()
                   );
                    pushBODE(data, "update");
                    showTop();
                    grbNoiDung.Enabled = false;
                    MessageBox.Show("Sửa thành công", "", MessageBoxButtons.OK);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn thật sự có muốn xóa???", "Xác nhận", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                if (((DataRowView)bODEBindingSource[bODEBindingSource.Position])["MAGV"].ToString().Trim().CompareTo(Program.username.Trim()) != 0)
                {
                    MessageBox.Show("Bạn không thể xóa câu hỏi của người khác", "", MessageBoxButtons.OK);
                    return;
                }
                else
                {
                    try
                    {
                        vitri = bODEBindingSource.Position;
                        DataRowView items = (DataRowView)this.bODEBindingSource.List[vitri];
                        BODE data = new BODE(
                           Convert.ToInt16(items["CAUHOI"].ToString().Trim()),
                           items["MAMH"].ToString().Trim(),
                           items["TRINHDO"].ToString().Trim(),
                           items["NOIDUNG"].ToString().Trim(),
                           items["A"].ToString().Trim(),
                           items["B"].ToString().Trim(),
                           items["C"].ToString().Trim(),
                           items["D"].ToString().Trim(),
                           items["DAP_AN"].ToString().Trim(),
                           items["MAGV"].ToString().Trim()
                        );
                        pushBODE(data, "delete");
                        showTop();
                        bODEBindingSource.RemoveCurrent();
                        this.bODETableAdapter.Update(this.dS.BODE);
                        this.bODETableAdapter.Fill(this.dS.BODE);
                        MessageBox.Show("Xóa thành công", "", MessageBoxButtons.OK);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("XóaMess" + ex.Message, "", MessageBoxButtons.OK);

                    }
                }
            }
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            reloadData();
        }
        void reloadData()
        {
            this.bODETableAdapter.Fill(this.dS.BODE);
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            if (this.undoTarget.Count <= 0) return;
            UndoStack popData = popBODE();
            string action = popData.HANHDONG;
            switch (action)
            {
                case "insert":
                    int lenght = this.bODEBindingSource.List.Count;
                    for (int i = 0; i <= lenght; i++)
                    {
                        DataRowView items = (DataRowView)this.bODEBindingSource.List[i];
                        bool isCAUHOITrue = Convert.ToInt16(items["CAUHOI"]) == popData.data.CAUHOI;
                        bool isMAMHTrue = items["MAMH"].ToString().Trim().CompareTo(popData.data.MAMH) == 0;
                        bool isMAGVTrue = items["MAGV"].ToString().Trim().CompareTo(popData.data.MAGV) == 0;
                        if (isCAUHOITrue && isMAMHTrue && isMAGVTrue)
                        {
                            this.bODEBindingSource.RemoveAt(i);
                            this.bODETableAdapter.Update(this.dS.BODE);
                            break;
                        }
                    }
                    break;
                case "delete":
                    this.bODETableAdapter.Insert(
                        popData.data.CAUHOI,
                        popData.data.MAMH,
                        popData.data.TRINHDO,
                        popData.data.NOIDUNG,
                        popData.data.A,
                        popData.data.B,
                        popData.data.C,
                        popData.data.D,
                        popData.data.DAP_AN,
                        popData.data.MAGV
                        );
                    break;
                case "update":
                    this.showTop();
                    UndoStack popData0 = popBODE();
                    this.bODETableAdapter.Update(
                        Convert.ToInt32(popData0.data.CAUHOI),
                        popData0.data.MAMH,
                        popData0.data.TRINHDO,
                        popData0.data.NOIDUNG,
                        popData0.data.A,
                        popData0.data.B,
                        popData0.data.C,
                        popData0.data.D,
                        popData0.data.DAP_AN,
                        popData0.data.MAGV,
                        Convert.ToInt32(popData.data.CAUHOI),
                        popData.data.MAMH,
                        popData.data.TRINHDO,
                        popData.data.DAP_AN,
                        popData.data.MAGV
                        );
                    break;
                default:
                    break;
            }
            reloadData();
        }
    }
}
