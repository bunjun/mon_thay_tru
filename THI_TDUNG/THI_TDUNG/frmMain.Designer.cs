﻿namespace THI_TDUNG
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grbChuyenCoSo = new System.Windows.Forms.GroupBox();
            this.cmbChuyenCoSo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnThoat = new System.Windows.Forms.Button();
            this.btnXembangdiem = new System.Windows.Forms.Button();
            this.btnTaoLogin = new System.Windows.Forms.Button();
            this.btnDkThi = new System.Windows.Forms.Button();
            this.btnNhapde = new System.Windows.Forms.Button();
            this.btnThi = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbNhom = new System.Windows.Forms.Label();
            this.lbMa = new System.Windows.Forms.Label();
            this.lbTen = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dSPM = new THI_TDUNG.DSPM();
            this.v_DS_PHANMANHBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.v_DS_PHANMANHTableAdapter = new THI_TDUNG.DSPMTableAdapters.V_DS_PHANMANHTableAdapter();
            this.tableAdapterManager = new THI_TDUNG.DSPMTableAdapters.TableAdapterManager();
            this.grbChuyenCoSo.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dSPM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_DS_PHANMANHBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // grbChuyenCoSo
            // 
            this.grbChuyenCoSo.Controls.Add(this.cmbChuyenCoSo);
            this.grbChuyenCoSo.Controls.Add(this.label4);
            this.grbChuyenCoSo.Location = new System.Drawing.Point(332, 227);
            this.grbChuyenCoSo.Name = "grbChuyenCoSo";
            this.grbChuyenCoSo.Size = new System.Drawing.Size(469, 113);
            this.grbChuyenCoSo.TabIndex = 16;
            this.grbChuyenCoSo.TabStop = false;
            this.grbChuyenCoSo.Text = "Chuyển cơ sở";
            // 
            // cmbChuyenCoSo
            // 
            this.cmbChuyenCoSo.DataSource = this.v_DS_PHANMANHBindingSource;
            this.cmbChuyenCoSo.DisplayMember = "TENCN";
            this.cmbChuyenCoSo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbChuyenCoSo.FormattingEnabled = true;
            this.cmbChuyenCoSo.Location = new System.Drawing.Point(111, 40);
            this.cmbChuyenCoSo.Name = "cmbChuyenCoSo";
            this.cmbChuyenCoSo.Size = new System.Drawing.Size(302, 28);
            this.cmbChuyenCoSo.TabIndex = 2;
            this.cmbChuyenCoSo.ValueMember = "TENSERVER";
            this.cmbChuyenCoSo.SelectedIndexChanged += new System.EventHandler(this.cmbChuyenCoSo_SelectedIndexChanged_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "CƠ SỞ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnThoat);
            this.groupBox2.Controls.Add(this.btnXembangdiem);
            this.groupBox2.Controls.Add(this.btnTaoLogin);
            this.groupBox2.Controls.Add(this.btnDkThi);
            this.groupBox2.Controls.Add(this.btnNhapde);
            this.groupBox2.Controls.Add(this.btnThi);
            this.groupBox2.Location = new System.Drawing.Point(332, 28);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(535, 159);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lựa chọn";
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(417, 61);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(93, 39);
            this.btnThoat.TabIndex = 7;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click_1);
            // 
            // btnXembangdiem
            // 
            this.btnXembangdiem.Location = new System.Drawing.Point(221, 89);
            this.btnXembangdiem.Name = "btnXembangdiem";
            this.btnXembangdiem.Size = new System.Drawing.Size(151, 55);
            this.btnXembangdiem.TabIndex = 5;
            this.btnXembangdiem.Text = "Xem bảng điểm";
            this.btnXembangdiem.UseVisualStyleBackColor = true;
            this.btnXembangdiem.Click += new System.EventHandler(this.btnXembangdiem_Click_1);
            // 
            // btnTaoLogin
            // 
            this.btnTaoLogin.Location = new System.Drawing.Point(27, 89);
            this.btnTaoLogin.Name = "btnTaoLogin";
            this.btnTaoLogin.Size = new System.Drawing.Size(177, 55);
            this.btnTaoLogin.TabIndex = 4;
            this.btnTaoLogin.Text = "Tạo tài khoản";
            this.btnTaoLogin.UseVisualStyleBackColor = true;
            this.btnTaoLogin.Click += new System.EventHandler(this.btnTaoLogin_Click_1);
            // 
            // btnDkThi
            // 
            this.btnDkThi.Location = new System.Drawing.Point(221, 32);
            this.btnDkThi.Name = "btnDkThi";
            this.btnDkThi.Size = new System.Drawing.Size(151, 39);
            this.btnDkThi.TabIndex = 2;
            this.btnDkThi.Text = "Đăng kí thi";
            this.btnDkThi.UseVisualStyleBackColor = true;
            this.btnDkThi.Click += new System.EventHandler(this.btnDkThi_Click_1);
            // 
            // btnNhapde
            // 
            this.btnNhapde.Location = new System.Drawing.Point(111, 32);
            this.btnNhapde.Name = "btnNhapde";
            this.btnNhapde.Size = new System.Drawing.Size(93, 39);
            this.btnNhapde.TabIndex = 1;
            this.btnNhapde.Text = "Nhập đề";
            this.btnNhapde.UseVisualStyleBackColor = true;
            this.btnNhapde.Click += new System.EventHandler(this.btnNhapde_Click_1);
            // 
            // btnThi
            // 
            this.btnThi.Location = new System.Drawing.Point(27, 32);
            this.btnThi.Name = "btnThi";
            this.btnThi.Size = new System.Drawing.Size(62, 39);
            this.btnThi.TabIndex = 0;
            this.btnThi.Text = "Thi";
            this.btnThi.UseVisualStyleBackColor = true;
            this.btnThi.Click += new System.EventHandler(this.btnThi_Click_1);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbNhom);
            this.groupBox1.Controls.Add(this.lbMa);
            this.groupBox1.Controls.Add(this.lbTen);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(23, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(259, 159);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin";
            // 
            // lbNhom
            // 
            this.lbNhom.AutoSize = true;
            this.lbNhom.Location = new System.Drawing.Point(77, 110);
            this.lbNhom.Name = "lbNhom";
            this.lbNhom.Size = new System.Drawing.Size(24, 20);
            this.lbNhom.TabIndex = 5;
            this.lbNhom.Text = "sv";
            // 
            // lbMa
            // 
            this.lbMa.AutoSize = true;
            this.lbMa.Location = new System.Drawing.Point(77, 32);
            this.lbMa.Name = "lbMa";
            this.lbMa.Size = new System.Drawing.Size(58, 20);
            this.lbMa.TabIndex = 4;
            this.lbMa.Text = "ma000";
            // 
            // lbTen
            // 
            this.lbTen.AutoSize = true;
            this.lbTen.Location = new System.Drawing.Point(77, 70);
            this.lbTen.Name = "lbTen";
            this.lbTen.Size = new System.Drawing.Size(89, 20);
            this.lbTen.TabIndex = 3;
            this.lbTen.Text = "pham dung";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nhóm:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã:";
            // 
            // dSPM
            // 
            this.dSPM.DataSetName = "DSPM";
            this.dSPM.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // v_DS_PHANMANHBindingSource
            // 
            this.v_DS_PHANMANHBindingSource.DataMember = "V_DS_PHANMANH";
            this.v_DS_PHANMANHBindingSource.DataSource = this.dSPM;
            // 
            // v_DS_PHANMANHTableAdapter
            // 
            this.v_DS_PHANMANHTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = THI_TDUNG.DSPMTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 602);
            this.Controls.Add(this.grbChuyenCoSo);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmMain";
            this.Text = "frmMain";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.grbChuyenCoSo.ResumeLayout(false);
            this.grbChuyenCoSo.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dSPM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_DS_PHANMANHBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbChuyenCoSo;
        private System.Windows.Forms.ComboBox cmbChuyenCoSo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Button btnXembangdiem;
        private System.Windows.Forms.Button btnTaoLogin;
        private System.Windows.Forms.Button btnDkThi;
        private System.Windows.Forms.Button btnNhapde;
        private System.Windows.Forms.Button btnThi;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbNhom;
        private System.Windows.Forms.Label lbMa;
        private System.Windows.Forms.Label lbTen;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DSPM dSPM;
        private System.Windows.Forms.BindingSource v_DS_PHANMANHBindingSource;
        private DSPMTableAdapters.V_DS_PHANMANHTableAdapter v_DS_PHANMANHTableAdapter;
        private DSPMTableAdapters.TableAdapterManager tableAdapterManager;
    }
}