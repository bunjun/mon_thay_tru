﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace THI_TDUNG
{
    public partial class frmThi : Form
    {
        public List<String> cauhoi = new List<String>();
        private int vitri, baithi;
        private double diem;
        private int a, b, c;


        BindingSource source = new BindingSource();
        public frmThi()
        {
            InitializeComponent();
        }

        private void btnKetthuc_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            if (Program.mGroup == "SINHVIEN")
            {
                MessageBox.Show("Diem của sv " + Program.maSV + " : " + diem.ToString());
                ghiDiem();
                grbNoiDung.Enabled = false;
            }
            else
            {
                MessageBox.Show("Diem thi thử của bạn là : " + diem.ToString());
                btnBatdau.Enabled = btnCausau.Enabled = btnKetthuc.Enabled = btnCautruoc.Enabled = grbNoiDung.Enabled = false;
                grbTimer.Enabled = false;
            }
        }
        private void ghiDiem()
        {
            try
            {
                string strLenh = string.Format("EXEC sp_ghidiem '{0}','{1}','{2}','{3}',{4}", Program.maMH, Program.maSV, Program.lan, Program.ngay, diem);
                Program.myReader = Program.ExecSqlDataReader(strLenh);
                Program.myReader.Read();
                MessageBox.Show("Lưu điểm thành công");
            }
            catch
            {
                MessageBox.Show("Chưa lưu được điểm");
            }
        }


        private void btnCautruoc_Click(object sender, EventArgs e)
        {
            if (radioA.Checked == true)
            {
                cauhoi[vitri] = "A";
            }
            else if (radioB.Checked == true)
            {
                cauhoi[vitri] = "B";
            }
            else if (radioC.Checked == true)
            {
                cauhoi[vitri] = "C";
            }
            else if (radioD.Checked == true)
            {
                cauhoi[vitri] = "D";
            }
            else
            {
                cauhoi[vitri] = "";
            }

            source.MovePrevious();
            vitri = source.Position;// vị trí bắt đầu tư 0

            // Đổ dữ liệu ra
            string result = ((DataRowView)source[vitri])["NOIDUNG"].ToString();
            lbND.Text = result;

            string result1 = ((DataRowView)source[vitri])["A"].ToString();
            radioA.Text = result1;

            string result2 = ((DataRowView)source[vitri])["B"].ToString();
            radioB.Text = result2;

            string result3 = ((DataRowView)source[vitri])["C"].ToString();
            radioC.Text = result3;

            string result4 = ((DataRowView)source[vitri])["D"].ToString();
            radioD.Text = result4;

            string cautrc = ((DataRowView)source[vitri + 1])["DAP_AN"].ToString();

            String result6 = cauhoi[vitri];

            if (result6.Equals("A"))
            {
                radioA.Checked = true;
            }
            else if (result6.Equals("B"))
            {
                radioB.Checked = true;
            }
            else if (result6.Equals("C"))
            {
                radioC.Checked = true;
            }
            else if (result6.Equals("D"))
            {
                radioD.Checked = true;
            }
            else
            {
                //cauhoi.Add("");
                radioA.Checked = radioB.Checked = radioC.Checked = radioD.Checked = false;
            }
            if (cauhoi[vitri + 1] == cautrc)
            {
                diem = (float)diem + 0.25;
            }
            if (vitri == 0)
            {
                btnCautruoc.Enabled = false;
            }
        

    }

    private void btnCausau_Click(object sender, EventArgs e)
        {
            if (radioA.Checked == true)
            {
                cauhoi[vitri] = "A";
            }
            else if (radioB.Checked == true)
            {
                cauhoi[vitri] = "B";
            }
            else if (radioC.Checked == true)
            {
                cauhoi[vitri] = "C";
            }
            else if (radioD.Checked == true)
            {
                cauhoi[vitri] = "D";
            }
            else
            {
                cauhoi[vitri] = "";
            }
            source.MoveNext();
            vitri = source.Position;

            string result = ((DataRowView)source[vitri])["NOIDUNG"].ToString();
            lbND.Text = result;

            string result1 = ((DataRowView)source[vitri])["A"].ToString();
            radioA.Text = result1;

            string result2 = ((DataRowView)source[vitri])["B"].ToString();
            radioB.Text = result2;

            string result3 = ((DataRowView)source[vitri])["C"].ToString();
            radioC.Text = result3;

            string result4 = ((DataRowView)source[vitri])["D"].ToString();
            radioD.Text = result4;

            string result5 = ((DataRowView)source[vitri])["DAP_AN"].ToString();

            string cautrc = ((DataRowView)source[vitri - 1])["DAP_AN"].ToString();

            String result6 = cauhoi[vitri];

            if (result6.Equals("A"))
            {
                radioA.Checked = true;
            }
            else if (result6.Equals("B"))
            {
                radioB.Checked = true;
            }
            else if (result6.Equals("C"))
            {
                radioC.Checked = true;
            }
            else if (result6.Equals("D"))
            {
                radioD.Checked = true;
            }
            else
            {
                //cauhoi.Add("");
                radioA.Checked = radioB.Checked = radioC.Checked = radioD.Checked = false;
            }
            if (cauhoi[vitri - 1] == cautrc)
            {
                diem = (float)diem + 0.25;
            }
            if (vitri == Program.socau - 1)
            {
                MessageBox.Show("Đã đến câu cuối");
            }
       

    }

    private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            a = Int32.Parse(label2.Text); //miligiay
            b = Int32.Parse(label1.Text); //giay
            c = Int32.Parse(label3.Text); //phut
            a--;
            if (a < 0)
            {
                a = 59;
                b--;
                if (b < 0)
                {
                    b = 59;
                    c--;
                }
            }


            if (a < 10)
            {
                label2.Text = "0" + a;
            }
            else
                label2.Text = a + "";
            if (b < 10)
            {
                label1.Text = "0" + b;
            }
            else
                label1.Text = b + "";
            if (c < 10)
            {
                label3.Text = "0" + c;
            }
            else
                label3.Text = c + "";


            if (a == 0 && b == 0 && c == 0)
            {
                timer1.Stop();
                if (Program.mGroup == "SINHVIEN")
                {
                    MessageBox.Show("Hết giờ");
                    MessageBox.Show("Diem của sv " + Program.maSV + " : " + diem.ToString());
                    ghiDiem();
                }
                else
                {
                    MessageBox.Show("Diem thi thử của bạn là : " + diem.ToString());

                }

            }

    }

    private void frmThi_Load(object sender, EventArgs e)
        {
            dS.EnforceConstraints = false;
            // TODO: This line of code loads data into the 'dS.BANGDIEM' table. You can move, or remove it, as needed.
            this.bANGDIEMTableAdapter.Fill(this.dS.BANGDIEM);
            // TODO: This line of code loads data into the 'dS.BODE' table. You can move, or remove it, as needed.
            this.bODETableAdapter.Fill(this.dS.BODE);

            getBoDe_SUA(Program.maMH, Program.socau, Program.trinhdo1);
            label3.Text = Program.thoigianthi.ToString();
            // MessageBox.Show(label3.Text);
            Random _r = new Random();
            baithi = _r.Next(1, 100);
            radioA.Enabled = radioB.Enabled = radioC.Enabled = radioD.Enabled = btnKetthuc.Enabled = false;
        }
        private void getBoDe_SUA(string mamh, int socau, string trinhdo)
        {


            string strLenh = string.Format("EXEC SP_LAYDETHI '{0}','{1}','{2}'", mamh, socau, trinhdo);
            source.DataSource = Program.ExecSqlDataTable(strLenh);

            vitri = source.Position;

            string result = ((DataRowView)source[vitri])["NOIDUNG"].ToString();
            lbND.Text = result;
            string result1 = ((DataRowView)source[vitri])["A"].ToString();
            radioA.Text = result1;
            string result2 = ((DataRowView)source[vitri])["B"].ToString();
            radioB.Text = result2;
            string result3 = ((DataRowView)source[vitri])["C"].ToString();
            radioC.Text = result3;
            string result4 = ((DataRowView)source[vitri])["D"].ToString();
            radioD.Text = result4;
            string result5 = ((DataRowView)source[vitri])["DAP_AN"].ToString();

            for (int i = 0; i < Program.socau; i++)
            {
                cauhoi.Add("");
            }


        }

        private void btnBatdau_Click(object sender, EventArgs e)
        {
            timer1.Start();
            radioA.Enabled = radioB.Enabled = radioC.Enabled = radioD.Enabled = btnKetthuc.Enabled = true;
        }
    }
}
