﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace THI_TDUNG
{
    public partial class frmDangNhap : DevExpress.XtraEditors.XtraForm
    {
        public frmDangNhap()
        {
            InitializeComponent();
        }

        private void frmDangNhap_Load(object sender, EventArgs e)
        {
            string chuoiketnoi = @"Data Source=DESKTOP-VRFGSJ2\SERVERGOC;Initial Catalog=TN_CSDLPT;Persist Security Info=True;User ID=sa;Password=123";
            Program.conn.ConnectionString = chuoiketnoi;
            Program.conn.Open();
            DataTable dt = new DataTable();
            dt = Program.ExecSqlDataTable("SELECT * FROM V_DS_PHANMANH");
            Program.bds_dspm.DataSource = dt;
            cmbCS.DataSource = dt;
            cmbCS.DisplayMember = "TENCN";
            cmbCS.ValueMember = "TENSERVER";

            cmbCS.SelectedIndex = -1;
            cmbCS.SelectedIndex = 0;
            txtLogin.Text = "pvh";
            txtPass.Text = "123";
            

            rdGV.Checked = true;
        }

        private void rdGV_CheckedChanged(object sender, EventArgs e)
        {
            if (rdGV.Checked == true)
            {
                txtPass.Enabled = true;
            }
        }

        private void rdSV_CheckedChanged(object sender, EventArgs e)
        {
            if (rdSV.Checked == true)
            {
                txtLogin.Clear();
                txtPass.Clear();
                txtPass.Enabled = false;
            }
        }

        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            if (rdSV.Checked == true)
            {
                if (txtLogin.Text.Trim() == "")
                {
                    MessageBox.Show("Login name không được trống", "", MessageBoxButtons.OK);
                    return;
                }
                Program.mlogin = "SV";
                Program.password = "SV";
                
                if (Program.KetNoi() == 0)
                    return;

                Program.mCoSo = cmbCS.SelectedIndex;

                // kiểm tra mã sinh viên mới nhập này tồn tại không
                string strLenh = "EXEC [dbo].[SP_TIMSV] @maSv = N'" + txtLogin.Text + "'";

                Program.myReader = Program.ExecSqlDataReader(strLenh);

                if (Program.myReader == null)
                    return;

                Program.username = txtLogin.Text;
                Program.myReader.Read();
                try
                {
                    Program.mHoten = Program.myReader.GetString(0);
                    Program.maSV = txtLogin.Text;
                }
                catch (Exception EX)
                {
                    MessageBox.Show("Không tồn tại sinh viên này", "", MessageBoxButtons.OK);
                    return;
                }

                Program.mGroup = "SINHVIEN";
                Program.conn.Close();
                MessageBox.Show("Đăng nhập thành công!", "", MessageBoxButtons.OK);
                // MessageBox.Show("Sinh viên: " + Program.mHoten + " - " + Program.mGroup, "", MessageBoxButtons.OK);

            }
            else
            {
                if (txtLogin.Text.Trim() == "" || txtPass.Text.Trim() == "")
                {
                    MessageBox.Show("Login name và mật mã không được trống", "", MessageBoxButtons.OK);
                    return;
                }
                Program.mlogin = txtLogin.Text;
                Program.password = txtPass.Text;
                if (Program.KetNoi() == 0) return;

                Program.mCoSo = cmbCS.SelectedIndex;

                Program.mloginDN = Program.mlogin;
                Program.passwordDN = Program.password;
                string strLenh = "EXEC SP_DANGNHAP '" + Program.mlogin + "'";

                Program.myReader = Program.ExecSqlDataReader(strLenh);
                if (Program.myReader == null) return;
                Program.myReader.Read();


                Program.username = Program.myReader.GetString(0);
                if (Convert.IsDBNull(Program.username))
                {
                    MessageBox.Show("Login bạn nhập không có quyền truy cập dữ liệu\n Bạn xem lại username, password", "", MessageBoxButtons.OK);
                    return;
                }
                Program.mHoten = Program.myReader.GetString(1);
                Program.mGroup = Program.myReader.GetString(2);
                Program.myReader.Close();
                Program.conn.Close();
                MessageBox.Show("Đăng nhập thành công!", "", MessageBoxButtons.OK);
                // MessageBox.Show("Nhan vien - Nhom : " + Program.mHoten + " - " + Program.mGroup, "", MessageBoxButtons.OK);
            }

            Program.frmChinh = new frmMain();
            this.Visible = false;
            Program.frmChinh.ShowDialog();
            this.Visible = true;
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbCS_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Program.servername = cmbCS.SelectedValue.ToString();

            }
            catch (Exception)
            {

            };
        }
    }
}
