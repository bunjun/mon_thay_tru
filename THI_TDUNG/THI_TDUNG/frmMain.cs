﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

 namespace THI_TDUNG
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.v_DS_PHANMANHTableAdapter.Connection.ConnectionString = Program.connstr;
            cmbChuyenCoSo.DataSource = Program.bds_dspm;
            cmbChuyenCoSo.SelectedIndex = Program.mCoSo;
            //this.v_DS_PHANMANHTableAdapter.Fill(this.dSPM.V_DS_PHANMANH);
            btnNhapde.Enabled = btnDkThi.Enabled = btnThi.Enabled = false;
            grbChuyenCoSo.Enabled = false;
            btnXembangdiem.Enabled = btnTaoLogin.Enabled = false;
            //groupbox hiện thông tin đăng nhập
            if (Program.mGroup != "SINHVIEN")
            {
                lbMa.Text = Program.username;
                lbTen.Text = Program.mHoten;
                lbNhom.Text = Program.mGroup;
            }
            if (Program.mGroup == "SINHVIEN")
            {
                lbMa.Text = Program.maSV;
                lbTen.Text = Program.mHoten;
                lbNhom.Text = Program.mGroup;
                //Sinh viên chỉ thi
                btnThi.Enabled = true;
                //không được tạo login
                btnTaoLogin.Enabled = false;

            }
            //set các button
            if (Program.mGroup == "GIAOVIEN")
            {
                //giang vien được nhập đề, đăng kí thi, thi thử không ghi điểm
                btnNhapde.Enabled = btnDkThi.Enabled = btnThi.Enabled = true;
                //không được tạo login
                btnTaoLogin.Enabled = false;
                //được xem bảng điểm câu 10
                btnXembangdiem.Enabled = true;

            }
            if (Program.mGroup == "COSO")
            {
                //giang vien được nhập đề, đăng kí thi, thi thử không ghi điểm
                btnNhapde.Enabled = btnDkThi.Enabled = btnThi.Enabled = true;
                //Được tạo login quyền CƠ SỞ và GIẢNG VIÊN
                btnTaoLogin.Enabled = true;
                btnXembangdiem.Enabled = true;

            }
            // CHỈ TRƯỜNG MỚI ĐƯỢC CHUYỂN CƠ SỞ
            if (Program.mGroup == "TRUONG")
            {
                grbChuyenCoSo.Enabled = true;

                Program.servername = cmbChuyenCoSo.SelectedValue.ToString();

                // chỉ được xem thôi nên vẫn cho chọn vào
                btnNhapde.Enabled = btnDkThi.Enabled = true;
                btnThi.Enabled = false;
                //Chỉ được xem báo cáo và tạo login
                btnTaoLogin.Enabled = true;
                btnXembangdiem.Enabled = true;
            }
        }

        private void btnThi_Click_1(object sender, EventArgs e)
        {
            Program.frmTruockhithi = new frmTruockhithi();
            this.Visible = false;
            Program.frmTruockhithi.ShowDialog();
            this.Visible = true;
        }

        private void btnNhapde_Click_1(object sender, EventArgs e)
        {
            Program.frmNhapDe = new frmNhapDe();
            this.Visible = false;
            Program.frmNhapDe.ShowDialog();
            this.Visible = true;
        }

        private void btnDkThi_Click_1(object sender, EventArgs e)
        {
            Program.servername = cmbChuyenCoSo.SelectedValue.ToString();
            MessageBox.Show("SV" + Program.servername);
            Program.frmDkThi = new frmDkThi();
            this.Visible = false;
            Program.frmDkThi.ShowDialog();
            this.Visible = true;
        }

        private void btnTaoLogin_Click_1(object sender, EventArgs e)
        {
            Program.servername = cmbChuyenCoSo.SelectedValue.ToString();
            Program.frmTaologin = new frmTaologin();
            this.Visible = false;
            Program.frmTaologin.ShowDialog();
            this.Visible = true;
        }

        private void btnXembangdiem_Click_1(object sender, EventArgs e)
        {
            Program.servername = cmbChuyenCoSo.SelectedValue.ToString();
            Program.frmXemBangDiem = new frmXemBangDiem();
            this.Visible = false;
            Program.frmXemBangDiem.ShowDialog();
            this.Visible = true;
        }

        private void btnThoat_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbChuyenCoSo_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            Program.servername = cmbChuyenCoSo.SelectedValue.ToString();
            if (cmbChuyenCoSo.SelectedIndex != Program.mCoSo)
            {
                Program.mlogin = Program.remoteLogin;
                Program.password = Program.remotePassword;

            }
            else
            {
                Program.mlogin = Program.mloginDN;
                Program.password = Program.passwordDN;

            }
         //   if (Program.KetNoi() == 0)
             //   MessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
        }
    }
}
