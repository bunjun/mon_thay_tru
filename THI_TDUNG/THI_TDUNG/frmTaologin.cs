﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace THI_TDUNG
{
    public partial class frmTaologin : Form
    {
        public frmTaologin()
        {
            InitializeComponent();
        }
        private string role()
        {
            string str = "";
            if (radTruong.Checked == true) str = "TRUONG";
            if (radCoso.Checked == true) str = "COSO";
            if (radGiangVien.Checked == true) str = "GIAOVIEN";
            return str;
        }

        private void frmTaologin_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dS.GIAOVIEN' table. You can move, or remove it, as needed.
            this.gIAOVIENTableAdapter.Fill(this.dS.GIAOVIEN);
            dS.EnforceConstraints = false;
            Program.connstr = "Data Source=" + Program.servername + ";Initial Catalog=" +
                     Program.database + ";User ID=" +
                     Program.mlogin + ";password=" + Program.password;
            this.gIAOVIENTableAdapter.Connection.ConnectionString = Program.connstr;
            this.gIAOVIENTableAdapter.Fill(this.dS.GIAOVIEN);
            // LIST QUYEN
            radCoso.Enabled = radGiangVien.Enabled = radTruong.Enabled = false;
            //truong chi tao dc cho truong
            if (Program.mGroup == "TRUONG")
            {
                radTruong.Enabled = true;
            }
            //co so tao quyen co so, giangvien
            if (Program.mGroup == "COSO")
            {
                radCoso.Enabled = radGiangVien.Enabled = true;
            }
            txtMATKHAU.PasswordChar = '*';
            txtXacnhan.PasswordChar = '*';
        }


        private void btnTaologin_Click_1(object sender, EventArgs e)
        {
            if (txtTAIKHOAN.Text.Trim() == null)
            {
                MessageBox.Show("Vui lòng nhập LoginName!", "", MessageBoxButtons.OK);
                txtTAIKHOAN.Focus();
                return;
            }
            if (txtMATKHAU.Text.Trim().CompareTo(txtXacnhan.Text.Trim()) != 0)
            {
                MessageBox.Show("Xác nhận mật khẩu không đúng!", "", MessageBoxButtons.OK);
                txtMATKHAU.Focus();
                return;
            }
            string strLenh = "DECLARE @result int " +
                            "EXEC @result = SP_TAOLOGIN N'"
                            + txtTAIKHOAN.Text + "', N'"
                            + txtMATKHAU.Text.Trim() + "', N'"
                            + cmbMAGV.SelectedValue.ToString() + "', N'"
                            + role() + "'" +
                            " SELECT 'result' = @result";
            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null) return;
            Program.myReader.Read();
            int result = int.Parse(Program.myReader.GetValue(0).ToString());
            Program.myReader.Close();
            if (result == 0)
            {
                MessageBox.Show("Tạo tài khoản thành công!", "", MessageBoxButtons.OK);
                return;
            }
            else
            {
                MessageBox.Show("Tạo tài khoản thất bại!", "", MessageBoxButtons.OK);
                return;
            }
        }

        private void radGiangVien_CheckedChanged_1(object sender, EventArgs e)
        {
            if (radGiangVien.Checked == true)
            {
                cmbMAGV.DataSource = gIAOVIENBindingSource;
                txtTAIKHOAN.Text = "";
                txtTAIKHOAN.ReadOnly = false;
            }
        }

        private void radCoso_CheckedChanged_1(object sender, EventArgs e)
        {

            if (radCoso.Checked == true)
            {
                cmbMAGV.DataSource = gIAOVIENBindingSource;
                txtTAIKHOAN.Text = "";
                txtTAIKHOAN.ReadOnly = false;
            }
        }

        private void radTruong_CheckedChanged_1(object sender, EventArgs e)
        {

            if (radTruong.Checked == true)
            {
                cmbMAGV.DataSource = gIAOVIENBindingSource;
                txtTAIKHOAN.Text = "";
                txtTAIKHOAN.ReadOnly = false;
            }
        }

        private void btnThoat_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
