﻿namespace THI_TDUNG
{
    partial class frmTruockhithi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lANLabel1;
            System.Windows.Forms.Label nGAYTHILabel1;
            System.Windows.Forms.Label mALOPLabel;
            System.Windows.Forms.Label tENMHLabel;
            this.grbLuaChon = new System.Windows.Forms.GroupBox();
            this.seLAN = new DevExpress.XtraEditors.SpinEdit();
            this.gIAOVIEN_DANGKYBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dS = new THI_TDUNG.DS();
            this.deNGAYTHI = new DevExpress.XtraEditors.DateEdit();
            this.cmbMalop = new System.Windows.Forms.ComboBox();
            this.btnThoat = new System.Windows.Forms.Button();
            this.btnThi = new System.Windows.Forms.Button();
            this.cbMAMH = new System.Windows.Forms.ComboBox();
            this.dSGVDKBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grbThongtin = new System.Windows.Forms.GroupBox();
            this.lbtenlop = new System.Windows.Forms.Label();
            this.lbmalop = new System.Windows.Forms.Label();
            this.lbhoten = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gIAOVIEN_DANGKYTableAdapter = new THI_TDUNG.DSTableAdapters.GIAOVIEN_DANGKYTableAdapter();
            this.tableAdapterManager = new THI_TDUNG.DSTableAdapters.TableAdapterManager();
            this.dS_GVDKTableAdapter = new THI_TDUNG.DSTableAdapters.DS_GVDKTableAdapter();
            lANLabel1 = new System.Windows.Forms.Label();
            nGAYTHILabel1 = new System.Windows.Forms.Label();
            mALOPLabel = new System.Windows.Forms.Label();
            tENMHLabel = new System.Windows.Forms.Label();
            this.grbLuaChon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seLAN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gIAOVIEN_DANGKYBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNGAYTHI.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNGAYTHI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSGVDKBindingSource)).BeginInit();
            this.grbThongtin.SuspendLayout();
            this.SuspendLayout();
            // 
            // lANLabel1
            // 
            lANLabel1.AutoSize = true;
            lANLabel1.Location = new System.Drawing.Point(792, 46);
            lANLabel1.Name = "lANLabel1";
            lANLabel1.Size = new System.Drawing.Size(40, 20);
            lANLabel1.TabIndex = 33;
            lANLabel1.Text = "LẦN";
            // 
            // nGAYTHILabel1
            // 
            nGAYTHILabel1.AutoSize = true;
            nGAYTHILabel1.Location = new System.Drawing.Point(482, 47);
            nGAYTHILabel1.Name = "nGAYTHILabel1";
            nGAYTHILabel1.Size = new System.Drawing.Size(85, 20);
            nGAYTHILabel1.TabIndex = 32;
            nGAYTHILabel1.Text = "NGÀY THI";
            // 
            // mALOPLabel
            // 
            mALOPLabel.AutoSize = true;
            mALOPLabel.Location = new System.Drawing.Point(18, 112);
            mALOPLabel.Name = "mALOPLabel";
            mALOPLabel.Size = new System.Drawing.Size(72, 20);
            mALOPLabel.TabIndex = 29;
            mALOPLabel.Text = "MÃ LỚP:";
            // 
            // tENMHLabel
            // 
            tENMHLabel.AutoSize = true;
            tENMHLabel.Location = new System.Drawing.Point(18, 48);
            tENMHLabel.Name = "tENMHLabel";
            tENMHLabel.Size = new System.Drawing.Size(88, 20);
            tENMHLabel.TabIndex = 0;
            tENMHLabel.Text = "MÔN HỌC:";
            // 
            // grbLuaChon
            // 
            this.grbLuaChon.Controls.Add(this.seLAN);
            this.grbLuaChon.Controls.Add(this.deNGAYTHI);
            this.grbLuaChon.Controls.Add(lANLabel1);
            this.grbLuaChon.Controls.Add(nGAYTHILabel1);
            this.grbLuaChon.Controls.Add(mALOPLabel);
            this.grbLuaChon.Controls.Add(this.cmbMalop);
            this.grbLuaChon.Controls.Add(this.btnThoat);
            this.grbLuaChon.Controls.Add(this.btnThi);
            this.grbLuaChon.Controls.Add(tENMHLabel);
            this.grbLuaChon.Controls.Add(this.cbMAMH);
            this.grbLuaChon.Location = new System.Drawing.Point(96, 204);
            this.grbLuaChon.Name = "grbLuaChon";
            this.grbLuaChon.Size = new System.Drawing.Size(1006, 219);
            this.grbLuaChon.TabIndex = 8;
            this.grbLuaChon.TabStop = false;
            this.grbLuaChon.Text = "Lựa chọn";
            // 
            // seLAN
            // 
            this.seLAN.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.gIAOVIEN_DANGKYBindingSource, "LAN", true));
            this.seLAN.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.seLAN.Location = new System.Drawing.Point(838, 43);
            this.seLAN.Name = "seLAN";
            this.seLAN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.seLAN.Properties.IsFloatValue = false;
            this.seLAN.Properties.Mask.EditMask = "N00";
            this.seLAN.Properties.MaxValue = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.seLAN.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.seLAN.Size = new System.Drawing.Size(72, 28);
            this.seLAN.TabIndex = 36;
            // 
            // gIAOVIEN_DANGKYBindingSource
            // 
            this.gIAOVIEN_DANGKYBindingSource.DataMember = "GIAOVIEN_DANGKY";
            this.gIAOVIEN_DANGKYBindingSource.DataSource = this.dS;
            // 
            // dS
            // 
            this.dS.DataSetName = "DS";
            this.dS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // deNGAYTHI
            // 
            this.deNGAYTHI.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.gIAOVIEN_DANGKYBindingSource, "NGAYTHI", true));
            this.deNGAYTHI.EditValue = null;
            this.deNGAYTHI.Location = new System.Drawing.Point(573, 43);
            this.deNGAYTHI.Name = "deNGAYTHI";
            this.deNGAYTHI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNGAYTHI.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNGAYTHI.Size = new System.Drawing.Size(150, 28);
            this.deNGAYTHI.TabIndex = 35;
            // 
            // cmbMalop
            // 
            this.cmbMalop.DataSource = this.gIAOVIEN_DANGKYBindingSource;
            this.cmbMalop.DisplayMember = "MALOP";
            this.cmbMalop.FormattingEnabled = true;
            this.cmbMalop.Location = new System.Drawing.Point(109, 109);
            this.cmbMalop.Name = "cmbMalop";
            this.cmbMalop.Size = new System.Drawing.Size(121, 28);
            this.cmbMalop.TabIndex = 30;
            this.cmbMalop.ValueMember = "MALOP";
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(545, 152);
            this.btnThoat.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(112, 35);
            this.btnThoat.TabIndex = 29;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click_1);
            // 
            // btnThi
            // 
            this.btnThi.Location = new System.Drawing.Point(372, 152);
            this.btnThi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnThi.Name = "btnThi";
            this.btnThi.Size = new System.Drawing.Size(112, 35);
            this.btnThi.TabIndex = 28;
            this.btnThi.Text = "Thi";
            this.btnThi.UseVisualStyleBackColor = true;
            this.btnThi.Click += new System.EventHandler(this.btnThi_Click_1);
            // 
            // cbMAMH
            // 
            this.cbMAMH.DataSource = this.dSGVDKBindingSource;
            this.cbMAMH.DisplayMember = "MAMH";
            this.cbMAMH.FormattingEnabled = true;
            this.cbMAMH.Location = new System.Drawing.Point(109, 43);
            this.cbMAMH.Name = "cbMAMH";
            this.cbMAMH.Size = new System.Drawing.Size(329, 28);
            this.cbMAMH.TabIndex = 1;
            this.cbMAMH.ValueMember = "MAMH";
            // 
            // dSGVDKBindingSource
            // 
            this.dSGVDKBindingSource.DataMember = "DS_GVDK";
            this.dSGVDKBindingSource.DataSource = this.dS;
            // 
            // grbThongtin
            // 
            this.grbThongtin.Controls.Add(this.lbtenlop);
            this.grbThongtin.Controls.Add(this.lbmalop);
            this.grbThongtin.Controls.Add(this.lbhoten);
            this.grbThongtin.Controls.Add(this.label3);
            this.grbThongtin.Controls.Add(this.label2);
            this.grbThongtin.Controls.Add(this.label1);
            this.grbThongtin.Location = new System.Drawing.Point(51, 27);
            this.grbThongtin.Name = "grbThongtin";
            this.grbThongtin.Size = new System.Drawing.Size(403, 160);
            this.grbThongtin.TabIndex = 7;
            this.grbThongtin.TabStop = false;
            this.grbThongtin.Text = "Thông tin";
            // 
            // lbtenlop
            // 
            this.lbtenlop.AutoSize = true;
            this.lbtenlop.Location = new System.Drawing.Point(108, 113);
            this.lbtenlop.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbtenlop.Name = "lbtenlop";
            this.lbtenlop.Size = new System.Drawing.Size(51, 20);
            this.lbtenlop.TabIndex = 30;
            this.lbtenlop.Text = "label6";
            // 
            // lbmalop
            // 
            this.lbmalop.AutoSize = true;
            this.lbmalop.Location = new System.Drawing.Point(108, 77);
            this.lbmalop.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbmalop.Name = "lbmalop";
            this.lbmalop.Size = new System.Drawing.Size(51, 20);
            this.lbmalop.TabIndex = 29;
            this.lbmalop.Text = "label5";
            // 
            // lbhoten
            // 
            this.lbhoten.AutoSize = true;
            this.lbhoten.Location = new System.Drawing.Point(108, 41);
            this.lbhoten.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbhoten.Name = "lbhoten";
            this.lbhoten.Size = new System.Drawing.Size(51, 20);
            this.lbhoten.TabIndex = 28;
            this.lbhoten.Text = "label4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 113);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 20);
            this.label3.TabIndex = 27;
            this.label3.Text = "Tên Lớp:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 77);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 20);
            this.label2.TabIndex = 26;
            this.label2.Text = "Mã Lớp:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 41);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 20);
            this.label1.TabIndex = 25;
            this.label1.Text = "Họ và tên:";
            // 
            // gIAOVIEN_DANGKYTableAdapter
            // 
            this.gIAOVIEN_DANGKYTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BANGDIEMTableAdapter = null;
            this.tableAdapterManager.BODETableAdapter = null;
            this.tableAdapterManager.COSOTableAdapter = null;
            this.tableAdapterManager.GIAOVIEN_DANGKYTableAdapter = this.gIAOVIEN_DANGKYTableAdapter;
            this.tableAdapterManager.GIAOVIENTableAdapter = null;
            this.tableAdapterManager.KHOATableAdapter = null;
            this.tableAdapterManager.LOPTableAdapter = null;
            this.tableAdapterManager.MONHOCTableAdapter = null;
            this.tableAdapterManager.SINHVIENTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = THI_TDUNG.DSTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // dS_GVDKTableAdapter
            // 
            this.dS_GVDKTableAdapter.ClearBeforeFill = true;
            // 
            // frmTruockhithi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1209, 501);
            this.Controls.Add(this.grbLuaChon);
            this.Controls.Add(this.grbThongtin);
            this.Name = "frmTruockhithi";
            this.Text = "frmTruockhithi";
            this.Load += new System.EventHandler(this.frmTruockhithi_Load);
            this.grbLuaChon.ResumeLayout(false);
            this.grbLuaChon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seLAN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gIAOVIEN_DANGKYBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNGAYTHI.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNGAYTHI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSGVDKBindingSource)).EndInit();
            this.grbThongtin.ResumeLayout(false);
            this.grbThongtin.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbLuaChon;
        private System.Windows.Forms.ComboBox cmbMalop;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Button btnThi;
        private System.Windows.Forms.ComboBox cbMAMH;
        private System.Windows.Forms.GroupBox grbThongtin;
        private System.Windows.Forms.Label lbtenlop;
        private System.Windows.Forms.Label lbmalop;
        private System.Windows.Forms.Label lbhoten;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DS dS;
        private System.Windows.Forms.BindingSource gIAOVIEN_DANGKYBindingSource;
        private DSTableAdapters.GIAOVIEN_DANGKYTableAdapter gIAOVIEN_DANGKYTableAdapter;
        private DSTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraEditors.SpinEdit seLAN;
        private DevExpress.XtraEditors.DateEdit deNGAYTHI;
        private System.Windows.Forms.BindingSource dSGVDKBindingSource;
        private DSTableAdapters.DS_GVDKTableAdapter dS_GVDKTableAdapter;
    }
}